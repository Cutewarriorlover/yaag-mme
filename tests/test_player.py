import json
import os
import sys
import unittest
from unittest.mock import patch, Mock

sys.path.insert(0, os.path.abspath("../src"))

from src.player import Player, getPlayer
from src.armor import ChainArmor
from src.weapons import StoneSword
from src.errors import PlayerAlreadyHasItemError


class TestPlayer(unittest.TestCase):
    def setUp(self):
        self.player = Player("test_player")

        # So it's clear the test is finished
        print("\n\n--------------------Next test--------------------\n\n")

    @patch("builtins.input", side_effect=["no", " ", "yes", "LLORI"])
    def test_get_player(self, mock_input):
        class MockGame:
            def __init__(self):
                self.state = {
                    "screen": None,
                    "heroName": ""
                }

        getPlayer.input = mock_input
        with open("../src/config.json") as f:
            self.assertEqual(getPlayer(MockGame(), json.load(f)["codes"]).name, Player("Arcaxer").name)

    def test_player_inventory(self):
        self.player.inventory.add(StoneSword())
        self.player.inventory.add(ChainArmor())
        self.player.print_inventory()

    def test_player_already_has_item_error(self):
        self.player.inventory.add(StoneSword())
        self.player.inventory.add(ChainArmor())
        self.assertRaises(PlayerAlreadyHasItemError, self.player.inventory.add, StoneSword())


if __name__ == '__main__':
    unittest.main(verbosity=2)
