src
===

.. toctree::
   :maxdepth: 4

   enums
   game
   player
   printer
   utils
