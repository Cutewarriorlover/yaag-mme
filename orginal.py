import random

titleScreen = "true"
game = "true"

roomNumber = 0
defense = 1
power = 1
trust = 0
luck = 0
killPoints = 0
basePlayerHP = 10
extraPlayerHP = 0
maximumHP = basePlayerHP + extraPlayerHP
playerHealth = maximumHP
HPRemaining = playerHealth

room14Monster = "alive"
room5Monster = "alive"
room9Monster = "alive"

room14Chest = "unlooted"
roomSecretChest = "unlooted"
roomSecret = "lost"

firstKill = "true"
deathCount = 0

tearState = "alive"
eeveeState = "alive"
dinoState = "alive"
murdleState = "alive"

epilogueID = 0


while titleScreen == "true":
  print("To play this game, you must type your answers. Also, to advance the story, press enter.")
  print("")
  input("Press enter to begin.")
  heroChoice = "true"
  while heroChoice == "true":
    print("Have you played Yet Another Adventure Game?")
    heroDecider = input("Yes or No? ")
    if heroDecider.lower() == "yes":
      heroNameToBe = input("What was your name in that game? ")
      if heroNameToBe.lower() == " ":
        print("Your name could not be blank. Please enter a different name.")
        print("")
      elif heroNameToBe.lower() == "":
        print("Your name could not be blank. Please enter a different name.")
        print("")
      else:
        print("Thank you.")
        input()
        heroName = heroNameToBe
        heroChoice = "false"
    elif heroDecider.lower() == "no":
      print("Thank you.")
      input()
      heroName = "WarriorGold001"
      heroChoice = "false"
    else:
      print("Please select a valid option.")
  codeChoice = "true"
  while codeChoice == "true":
    print("Do you have a character code?")
    characterCode = input("Yes or No? ")
    if characterCode.lower() == "yes":
      code = input("What is your code? ")
      if code == "EATXZ":
        print("Welcome, MurdleMuffin.")
        playerName = "MurdleMuffin"
        titleScreen = "false"
        codeChoice = "false"
      elif code == "LLORI":
        print("Welcome, Arcaxer.")
        playerName = "Arcaxer"
        titleScreen = "false"
        codeChoice = "false"
      elif code == "SKBHX":
        print("Welcome, CoenPlayz.")
        playerName = "CoenPlayz"
        titleScreen = "false"
        codeChoice = "false"
      elif code == "PIKTS":
        print("Welcome, Eevee005.")
        playerName = "Eevee005"
        titleScreen = "false"
        codeChoice = "false"
      elif code == "FRMPO":
        print("Welcome, Potato Man.")
        playerName = "Potato Man"
        titleScreen = "false"
        codeChoice = "false"
      elif code == "DEVYT":
        print("Welcome, DinoPackYT.")
        playerName = "DinoPackYT"
        titleScreen = "false"
        codeChoice = "false"
      elif code == "DIDEV":
        print("Welcome, Dino-Pack.")
        playerName = "Dino-Pack"
        titleScreen = "false"
        codeChoice = "false"
      elif code == "BAD4U":
        print("Welcome, Tear 2bad.")
        playerName = "Tear 2bad"
        titleScreen = "false"
        codeChoice = "false"
      else:
        print("That's not a valid code.")
        print("")
    elif characterCode.lower() == "no":
      playerName = input("What is your name? ")
      if playerName.lower() == "dino-pack":
        print("Hey! That's me. You can't take that name.")
        print("")
        print("Try again.")
        print("")
      elif playerName.lower() == "eevee005":
        print("That name is taken, please try again.")
        print("")
      elif playerName.lower() == "murdlemuffin":
        print("That name is taken, please try again.")
        print("")
      elif playerName.lower() == "tear 2bad":
        print("That name is taken, please try again.")
        print("")
      elif playerName.lower() == heroName.lower():
        print("That name is taken, please try again.")
        print("")
      elif playerName.lower() == " ":
        print("Your name cannot be blank. Please enter a different name.")
        print("")
      elif playerName.lower() == "":
        print("Your name cannot be blank. Please enter a different name.")
        print("")
      else:
        titleScreen = "false"
        codeChoice = "false"

partyMembers = ["Dino-Pack", "MurdleMuffin",
                "Tear 2bad", "Eevee005", playerName]
while game == "true":
  if roomNumber == 0:
    print("")
    print("You spawn in the dungeon with four other players. A big sign is above the door into the first room, it lists all the players' names.")
    print("You read the sign, it reads:")
    print("Dino-Pack, Eevee005, Tear 2bad, MurdleMuffin and %s." % playerName)
    print("")
    print("")
    print("'So those are the others I'm playing with, huh?' You think to yourself.")
    input()
    print("You feel a tap on your shoulder.")
    input("")
    print("'Hi! My name is Dino-Pack, I'm just going around, introducing myself to everyone. I can't wait to start!' says the man standing in front of you, assumingly the one who tapped you.")
    input("")
    print("You look at him for a second. He's got short brown hair with a matching eye color. He's wearing a white hoodie, and standard brown pants. He's wearing glasses, but you can see they're dirty. He's slightly taller than you are.")
    input()
    print("'So, what's your name?' asks Dino-Pack. 'Oh! my name is %s.' you answer." % playerName)
    input()
    print("'Nice to meet you %s! Come on, let's go meet the others.'" % playerName)
    input()
    print("'Uhh, sure...' you say, following Dino-Pack.")
    input()
    print("You both walk up to a woman, reading something on a peice of paper.")
    input()
    print("'Hello?' says Dino-Pack to the woman. She looks up. 'Oh, hi! Nice to meet you two, my name is Eevee005, yours?' She asks with a smile. 'My name's Dino-Pack, it's nice to meet you!' he says. 'My name's %s.' you reply simply." % playerName)
    input()
    print("'She's quite charming,' you think to yourself. She's also got brown hair, but it goes down about a quarter of her back. She has blue eyes, and is also wearing a hoodie, but a blue one. She's slightly shorter than Dino-Pack, but about the same height as you.")
    input()
    print("'Have you two noticed how complicated this game is? I'm reading this player guide, and it talks about so many mechanics.' Eevee005 says, holding up her paper. 'No, I haven't read anything about this game yet, I wanted to meet everyone first.' replies Dino-Pack.")
    input()
    print("'Speaking of, we should go meet the others.' suggests Dino-Pack. 'Good idea, but who should we speak to first?' says Eevee005. 'How about %s decides?' says Dino-Pack, getting a nod from Eevee005." % playerName)
    input("")
    print("---Decision Time---")
    print("")
    print("This game runs on decisions. This one doesn't change your story, but it acts as a tutorial. Future choices will be remembered.")
    print("")
    muffinOrTearChoice = "true"
    while muffinOrTearChoice == "true":
      print("Who will you speak to?")
      muffinOrTear = input(
          "1 - The guy sitting in the corner, waiting. 2 - The guy geting himself ready for the dungeon run.")
      if muffinOrTear == "1":
        print("")
        print(
            "'How about the guy in the corner?' you say. 'Good choice.' replies Eevee005.")
        input()
        print("You, Dino-Pack, and Eevee005 walk up to the guy sitting down in the corner. Eevee005 speaks first.")
        input()
        print("'Hi, I'm Eevee005, this is Dino-Pack, and this is %s.' she says, pointing you both out as she calls your names." % playerName)
        input()
        print("'Oh, uhh, hi?' says the guy, sounding confused. 'My name's MurdleMuffin. When do we start this thing?' he continued. 'At 12:25.' replied Eevee005. 'And it's nice to meet you, MurdleMuffin.' Dino-Pack added.")
        input()
        print("You can't tell much about what MurdleMuffin looks like. He's wearing a black hood, and you can't see his hair, but you assume it's short for it to not be seen. Because he's sitting down, his height is also unknown. 'What a mysterious guy...' You think.")
        input()
        print("You glance at a clock. 12:15. 'Damn, still 10 more minutes.' you think to yourself.")
        input()
        print("'Now all there's left is the other guy.' you remark. 'Indeed. We should go say hi.' says Eevee005. 'Oh, I'm out. I'd rather sit here for 10 minutes. I don't think I'm ready yet.' responds MurdleMuffin 'Suit yourself, we're going.' responds Dino-Pack, and you three walk over to the last guy.")
        input()
        print("At this point, you've figured out the name of the last guy. He's the one you haven't spoken to yet.")
        input()
        print("---Quiz Time---")
        print("")
        print("In this game, sometimes you'll encounter quizzes. Sometimes you need to answer them correctly to gain the trust of your party members. Other times, you get a stat boost.")
        print("")
        quiz1 = input("What is the name of the last guy? ")
        if quiz1.lower() == "tear 2bad":
          print("Correct! You get +1 HP")
          extraPlayerHP += 1
          maximumHP += 1
        elif quiz1.lower() == playerName.lower():
          print("They're not you...")
          print("That's incorrect.")
        else:
          print("That is incorrect. Sorry.")
        print("")
        input("Press enter to continue.")
        print("")
        print("You walk up to the guy, and he immediately speaks first.")
        input()
        print("'Hi! My name's Tear 2bad, I'm so exited for this dungeon run! Aren't you three exited too?' he rambles, exitedly. 'I guess I'm exited.' You say. 'We all are, to an extent.' adds Dino-Pack. 'Nice to meet you guys, what're your names?' asks Tear 2bad. 'Oh, my name's Dino-Pack, this is Eevee005, and this is %s.' replies Dino-Pack." % playerName)
        input()
        print("As much as he's moving around, you still get a good look at him. He's quite tall, taller than even Dino-Pack, if only just slightly. He's wearing a standard grey long sleeved t-shirt, and jeans. He has black hair, and green coloured eyes")
        input()
        print("'Cool, cool. Hey, hey, when do we start?' asks Tear 2bad. '12:25' replies Eevee005. 'Nice, nice. See you then.' declares Tear 2bad, walking away.")
        input()
        print("'That was... odd.' says Dino-Pack. 'Yeah, definately.' agrees Eevee005. 'Best we not question it, it's almost time to start.' continues Eevee005.")
        muffinOrTearChoice = "false"
      elif muffinOrTear == "2":
        print("")
        print("'How about the guy preparing over there?' you say. 'Good choice.' replies Eevee005.")
        input()
        print("You walk up to the guy, and he immediately speaks first.")
        input()
        print("'Hi! My name's Tear 2bad, I'm so exited for this dungeon run! Aren't you three exited too?' he rambles, exitedly. 'I guess I'm exited.' You say. 'We all are, to an extent.' adds Dino-Pack. 'Nice to meet you guys, what're your names?' asks Tear 2bad. 'Oh, my name's Dino-Pack, this is Eevee005, and this is %s.' replies Dino-Pack." % playerName)
        input()
        print("As much as he's moving around, you still get a good look at him. He's quite tall, taller than even Dino-Pack, if only just slightly. He's wearing a standard grey long sleeved t-shirt, and jeans. He has black hair, and green coloured eyes")
        input()
        print("'Cool, cool. Hey, hey, when do we start?' asks Tear 2bad. '12:25' replies Eevee005. 'Nice, nice. See you then.' declares Tear 2bad, walking away.")
        input()
        print("'That was... odd.' says Dino-Pack. 'Yeah, definately.' agrees Eevee005. 'Best we not question it, there's still one guy left.' continues Eevee005.")
        input()
        print("'Lets go pay him a visit.' says Dino-Pack.")
        input()
        print("At this point, you've figured out the name of the last guy. He's the one you haven't spoken to yet.")
        input()
        print("---Quiz Time---")
        print("")
        print("In this game, sometimes you'll encounter quizzes. Sometimes you need to answer them correctly to gain the trust of your party members. Other times, you get a stat boost.")
        print("")
        quiz1 = input("What is the name of the last guy? ")
        if quiz1.lower() == "murdlemuffin":
          print("Correct! You get +1 HP")
          extraPlayerHP += 1
          maximumHP += 1
        elif quiz1.lower() == playerName.lower():
          print("They're not you...")
          print("That's incorrect.")
        else:
          print("That is incorrect. Sorry.")
        print("")
        input("Press enter to continue.")
        print("")
        print("You, Dino-Pack, and Eevee005 walk up to the guy sitting down in the corner. Eevee005 speaks first.")
        input()
        print("'Hi, I'm Eevee005, this is Dino-Pack, and this is %s.' she says, pointing you both out as she calls your names." % playerName)
        input()
        print("'Oh, uhh, hi?' says the guy, sounding confused. 'My name's MurdleMuffin. When do we start this thing?' he continued. 'At 12:25.' replied Eevee005. 'And it's nice to meet you, MurdleMuffin.' Dino-Pack added.")
        input()
        print("You can't tell much about what MurdleMuffin looks like. He's wearing a black hood, and you can't see his hair, but you assume it's short for it to not be seen. Because he's sitting down, his height is also unknown. 'What a mysterious guy...' You think.")
        input()
        print("You glance at a clock. 12:15. 'Damn, still 10 more minutes.' you think to yourself.")
        input()
        muffinOrTearChoice = "false"
      else:
        print("That's not an option.")
        print("")
        print("Please select a valid option.")
        input()
    print("")
    print("10 minutes later...")
    input()
    print("A voice plays on what seems to be a speaker. 'Alright players, it's time to begin the first play test of Multiplayer Dungeon Runs here at Arm Attack Inc.!' says the loudspeaker voice.")
    input()
    print("'You will be given items upon spawning, they are one tier up from default starting gear.' continues the voice.")
    input()
    print("'Any questions? We can hear you, so just ask aloud.' asks the voice.")
    input()
    print("Silence.")
    input()
    print("'Alright then, prepare to be warped, and defeat Mr Dye!' says the voice, before you begin to feel floaty.")
    input()
    print("You close your eyes, and when you open the again, you're in the first room of the dunegon.")
    input()
    roomNumber = "start"
  elif roomNumber == "start":
    print("Communication has become video game chat style, instead of actual speaking as it was before.")
    input()
    print("---Chat---")
    print("Dino-Pack: oki were here")
    input()
    print("Eevee005: yeah")
    input()
    print("Tear 2bad: lets speedrun mr dye")
    input()
    print("MurdleMuffin: what? why would we do that?")
    input()
    print("Dino-Pack: murdle's right")
    input()
    print("Dino-Pack: we need a better plan than rushing")
    input()
    print("Time for you to speak in chat.")
    message = input("Type a message to send here: ")
    print(playerName, ":", message)
    input()
    print("Tear 2bad: %s that plan sucks" % playerName)
    input()
    print("Eevee005: lets just explore all the rooms, we can get good loot and finish off mr dye")
    input()
    print("Dino-Pack: good idea")
    input()
    print("MurdleMuffin: sounds like a plan")
    input()
    print("Tear 2bad: you guys are lame")
    message = input("Type a message to send here: ")
    print(playerName, ":", message)
    input()
    print("Dino-Pack: ok go time")
    print("---Chat has ended---")
    input()
    print("You got a Stone Sword!")
    input()
    print("You got Chain Armor!")
    input()
    defense = 3
    power += 2
    print("You watch as everyone runs out of the room.")
    input()
    roomChoice = "true"
    while roomChoice == "true":
      roomdecide = input("Where do you go? Right, or left? ")
      if roomdecide.lower() == "right":
        roomNumber = 2
        previousRoom = 1
        roomChoice = "false"
      elif roomdecide.lower() == "left":
        roomNumber = "tearKillRoom"
        previousRoom = 1
        roomChoice = "false"
      else:
        print("That's not a valid option.")
        print("")
  elif roomNumber == 1:
    print("As you walk back into the starting room, you notice how quiet and empty the room is.")
    input()
    print("'This was probably just used so we could spawn in...' you think, seeing as how there was nothing in the room.")
    input()
    print("Without anything to do, the best course of action would be to simply leave the room.")
    input()
    roomChoice = "true"
    while roomChoice == "true":
      roomdecide = input("Where do you go? Right, or left? ")
      if roomdecide.lower() == "right":
        roomNumber = 2
        previousRoom = 1
        roomChoice = "false"
      elif roomdecide.lower() == "left":
        if tearState == "alive":
          roomNumber = "tearKillRoom"
        else:
          roomNumber = 11
        previousRoom = 1
        roomChoice = "false"
      else:
        print("That's not a valid option.")
        print("")
  elif roomNumber == 2:
    if previousRoom == 1:
      print("You walk into the right-hand room.")
    elif previousRoom == 3:
      print("You walk back into the room.")
    else:
      print("You walk into the room.")
    input()
    if murdleState == "dead":
      print("To your right, MurdleMuffin body is still there, basically haunting you.")
      input()
    else:
      print("To your right, you see MurdleMuffin, sitting in a corner.")
      input()
    print("You should probably move along and do something useful.")
    input()
    roomChoice = "true"
    while roomChoice == "true":
      roomdecide = input("Where do you go? Right, left or back? ")
      if roomdecide.lower() == "left":
        roomNumber = 4
        previousRoom = 2
        roomChoice = "false"
      elif roomdecide.lower() == "right":
        if murdleState == "alive":
          roomNumber = "murdleKillRoom"
        else:
          roomNumber = 3
        previousRoom = 2
        roomChoice = "false"
      elif roomdecide.lower() == "back":
        previousRoom = 2
        roomNumber = 1
        roomChoice = "false"
      else:
        print("That's not a valid option.")
        print("")
  elif roomNumber == "murdleKillRoom":
    print("You walk into the room to your right.")
    input()
    print("MurdleMuffin is sitting in the corner, like you noticed before.")
    input()
    print("---Chat---")
    input()
    print("%s: hello" % playerName)
    input()
    print("MurdleMuffin: oh")
    input()
    print("MurdleMuffin: hi")
    input()
    print("%s: what are you doing?" % playerName)
    input()
    print("MurdleMuffin: sitting in a corner")
    input()
    print("%s: i can see that" % playerName)
    input()
    print("%s: but why" % playerName)
    input()
    print("%s: shouldnt you be helping?" % playerName)
    input()
    print("MurdleMuffin: i didnt really want to do this")
    input()
    print("MurdleMuffin: i just wanted the money")
    input()
    print("%s: oh i see" % playerName)
    input()
    print("%s: well have fun i guess" % playerName)
    input()
    print("MurdleMuffin: thanks")
    input()
    print("MurdleMuffin: wait")
    input()
    print("%s: ?" % playerName)
    input()
    print("MurdleMuffin: take this, i wont need it")
    input()
    print("%s: thank you" % playerName)
    input()
    print("---Chat has ended---")
    input()
    print("You got an HP Restore!")
    playerHP = maximumHP
    input()
    print("You are now at %s total HP!" % playerHP)
    print("---Spare, or kill?---")
    input()
    if firstKill == "true":
     print("In this game, you have the choice to either be a murderer, or run the dungeon as normal. Most of the time, you only get one chance to kill your party members, so choose wisely.")
     firstKill = "false"
    input()
    killChoice = "true"
    while killChoice == "true":
      killMurdle = input(
          "Will you kill MurdleMuffin? Please enter 'yes' or 'no': ")
      if killMurdle.lower() == "no":
        print("You did not kill MurdleMuffin.")
        print("")
        print("Trust was raised by 10!")
        murdleState = "spared"
        trust += 10
        killChoice = "false"
      elif killMurdle.lower() == "yes":
        print("You turn around to face MurdleMuffin, and draw your sword.")
        input()
        print("---Chat---")
        input()
        print("MurdleMuffin: what are you doing?")
        input()
        print("MurdleMuffin: is there a monster around?")
        input()
        print("%s: no :)" % playerName)
        input()
        print("MurdleMuffin: then why do you have your sword?")
        input()
        print("%s: say goodbye" % playerName)
        input()
        print("MurdleMuffin: what?")
        input()
        print("---Chat has ended---")
        input()
        print("---Battle!---")
        input()
        battle = "true"
        playerHealth = HPRemaining
        playerHealing = 4
        monsterHP = 11
        monsterDefense = 2
        monsterHealing = 2
        maxPlayerHealth = playerHealth
        print("You initiated a battle!")
        print("")
        while playerHealth > 0:
          while battle == "true":
            turn = input("What do you do? 1-Attack, 2-Defend, 3-Heal")
            if turn == "1":
              damage = power - monsterDefense
              monsterHP = monsterHP - damage
              print("You dealt", damage, "damage to MurdleMuffin!")
            elif turn == "2":
              print("You defended! But MurdleMuffin doesn't attack.")
            elif turn == "3":
              healER = random.randint(1, 4)
              if healER > 1:
                if playerHealth + playerHealing > maxPlayerHealth:
                  playerHealth = maxPlayerHealth
                  print("You healed yourself to max HP!")
                else:
                  playerHealth = playerHealth + playerHealing
                print("You healed yourself for", playerHealing, "HP!")
              else:
                print("Your heal failed.")
            else:
              print("That's not a valid option?")
            print("")
            print("You have", playerHealth, "HP remaining!")
            print("")
            print("MurdleMuffin has", monsterHP, "HP remaining!")
            if playerHealth <= 0:
              HPRemaining = 0
              battle = "false"
            if monsterHP <= 0:
              print("You defeated MurdleMuffin!")
              battle = "false"
            else:
              continue
          HPRemaining = playerHealth
          playerHealth = 0
        if HPRemaining <= 0:
          game = "false"
          gameOver = "true"
        playerHealth = HPRemaining
        print("The battle ends and you have", playerHealth, "HP remaining!")
        input()
        print("---Chat---")
        print()
        print("MurdleMuffin: ack")
        input()
        print("MurdleMuffin: my defense was too low")
        input()
        print("MurdleMuffin: tell me why are you doing this %s" % playerName)
        input()
        message = input("Why do you want to kill MurdleMuffin?")
        print("")
        print(playerName, ":", message)
        input()
        print("MurdleMuffin: awh man")
        input()
        print("%s: you forgot a word" % playerName)
        input()
        print("MurdleMuffin: what?")
        input()
        print("%s: creeper" % playerName)
        input()
        print("MurdleMuffin: ...")
        input()
        print("%s: any last words?" % playerName)
        input()
        print("MurdleMuffin: this is why i didnt want to play this game...")
        input()
        print("---Chat has ended---")
        input()
        print("You swing your sword at MurdleMuffin, killing him.")
        input()
        murdleState = "dead"
        killPoints += 5
        killChoice = "false"
        deathCount += 1
    if murdleState == "dead":
      print("You stand there, deciding to leave the room.")
      input()
    elif murdleState == "spared":
      print("Walking away, you decide where you'll go.")
      input()
    print("You walk back into the room you were just in.")
    input()
    previousRoom = 3
    roomNumber = 2
  elif roomNumber == 3:
    print("You walk into the room where you've seen MurdleMuffin.")
    input()
    if murdleState == "spared":
      print("You notice MurdleMuffin is still sitting in the corner.")
      input()
      print("You should leave him alone.")
    elif murdleState == "dead":
      print("You see Murdle's body in the corner.")
      input()
      print("You should leave.")
    print("You walk back the way you came.")
    previousRoom = 3
    roomNumber = 2
  elif roomNumber == 4:
    print("You walk into the room.")
    input()
    print("It's a large hallway with nothing in it, so you continue.")
    input()
    if previousRoom == 2:
      roomNumber = 5
      previousRoom = 4
    elif previousRoom == 5:
      roomNumber = 3
      previousRoom = 4
  elif roomNumber == 5:
    print("You continue walking, and enter the room.")
    if room5Monster == "alive":
      print("You find a Slime in the corner!")
      input()
      print("---Battle!---")
      input()
      battle = "true"
      playerHealth = HPRemaining
      playerHealing = 4
      monsterHP = 6
      monsterDamage = 3
      monsterHealing = 0
      maxPlayerHealth = playerHealth
      print("You encountered a Slime!")
      print("")
      while playerHealth > 0:
        while battle == "true":
          turn = input("What do you do? 1-Attack, 2-Defend, 3-Heal")
          if turn == "1":
            monsterHP = monsterHP - power
            print("You dealt", power, "damage to the Slime!")
            if defense > monsterDamage:
              damage = 0
            else:
              damage = monsterDamage - defense
            playerHealth = playerHealth - damage
            print("")
            print("The Slime dealt", damage, "damage to you!")
          elif turn == "2":
            defended = defense + 5
            if defended > monsterDamage:
              damage = 0
            else:
              damage = monsterDamage - defended
            print("You defended!")
            print("")
            print("The Slime dealt", damage, "damage to you!")
          elif turn == "3":
            healER = random.randint(1, 4)
            if healER > 1:
              if playerHealth + playerHealing > maxPlayerHealth:
                playerHealth = maxPlayerHealth
                print("You healed yourself to max HP!")
              else:
                playerHealth = playerHealth + playerHealing
              print("You healed yourself for", playerHealing, "HP!")
            else:
              print("Your heal failed.")
            if defense > monsterDamage:
              damage = 0
            else:
              damage = monsterDamage - defense
            playerHealth = playerHealth - damage
            print("")
            print("The Slime dealt", damage, "damage to you!")
          else:
            print("That's not a valid option?")
          print("")
          print("You have", playerHealth, "HP remaining!")
          print("")
          print("The Slime has", monsterHP, "HP remaining!")
          if playerHealth <= 0:
            HPRemaining = 0
            battle = "false"
          if monsterHP <= 0:
            print("You defeated the Slime!")
            battle = "false"
          else:
            continue
          HPRemaining = playerHealth
          playerHealth = 0
        if HPRemaining <= 0:
          game = "false"
          gameOver = "true"
        print("The battle ends and you have", HPRemaining, "HP remaining!")
        input()
      playerHealth = HPRemaining
      print("The enemy dropped a stat boost!")
      input()
      loot = random.randint(1, 4)
      if loot == 1:
        print("You got a small HP Boost!")
        extraPlayerHP += 3
        maximumHP += 3
      elif loot == 2:
        print("You got a small Power Boost!")
        power += 1
      elif loot == 3:
        print("You got a small Defense Boost!")
        defense += 2
      elif loot == 4:
        print("You got a small Luck Boost!")
        luck += 1
      input()
      room5Monster = "dead"
    print("You look around the room, and don't find anything, so you keep walking.")
    input()
    if previousRoom == 4:
      previousRoom = 5
      roomNumber = 6
    elif previousRoom == 6:
      previousRoom = 5
      roomNumber = 4
  elif roomNumber == 6:
    print("You walk into the room, and something feels off.")
    input()
    print("You can do a check for any hidden items.")
    input()
    checkChoice = "true"
    while checkChoice == "true":
      check = input("Would you like to check for anything hidden? ")
      if check.lower() == "yes":
        checkroll = random.randint(1, 20)
        if checkroll <= 10:
          print("You found an HP Restore!")
          playerHP = maximumHP
          input()
          print("You are now at %s total HP!" % playerHP)
          checkChoice = "false"
        elif checkroll >= 11 & checkroll <= 18:
          print("You found a stat boost!")
          input()
          loot = random.randint(1, 4)
          if loot == 1:
            print("You got a small HP Boost!")
            extraPlayerHP += 3
            maximumHP += 3
            checkChoice = "false"
          elif loot == 2:
            print("You got a small Power Boost!")
            power += 1
            checkChoice = "false"
          elif loot == 3:
            print("You got a small Defense Boost!")
            defense += 2
            checkChoice = "false"
          elif loot == 4:
            print("You got a small Luck Boost!")
            luck += 1
            checkChoice = "false"
        elif checkroll >= 19:
          print("You found nothing.")
          input()
          checkChoice = "false"
      elif check.lower() == "no":
        print("You decide not to look around.")
        input()
        checkChoice = "false"
      else:
        print("That's not a valid option!")
        input()
    print("Your feeling of uneasiness is gone, for now.")
    input()
    print("You should keep going.")
    input()
    if previousRoom == 5:
      if dinoState == "alive":
        roomNumber = "dinoKillRoom"
      else:
        roomNumber = 7
      previousRoom = 6
    elif previousRoom == 7:
      roomNumber = 5
      previousRoom = 6
  elif roomNumber == "dinoKillRoom":
    print("As you walk in, Dino-Pack notices you've entered.")
    input()
    print("---Chat---")
    print("")
    print("Dino-Pack: %s! hi" % playerName)
    input()
    print("%s: hey" % playerName)
    input()
    print("Dino-Pack: have you seen Tear or Murdle?")
    input()
    print("---Luck Test---")
    print("")
    print("In this half of the game, you will have to perform Luck Tests. The tests could be used to get stat boosts, gear, or to throw off suspision.")
    input()
    lieDecide = "true"
    while lieDecide == "true":
      checker = random.randint(0, 20)
      luckCheck = luck + trust + killPoints
      lieOrTruth = input("Would you like to lie, or tell the truth? ")
      if lieOrTruth.lower() == "lie":
        if luckCheck >= checker:
          luckRoll = random.randint(1, 2)
          if luckRoll == 1:
            print("%s: uh well i dont think i saw them" % playerName)
            input()
            print("Dino-Pack: uh, what?")
            input()
            print("Dino-Pack: are you sure?")
            input()
            print("%s: yeah sure" % playerName)
            input()
            print("Dino-Pack: alright then...")
            lieDecide = "false"
          elif luckRoll == 2:
            print("%s: no, i didnt see them" % playerName)
            input()
            print("Dino-Pack: ah, okay")
            input()
            print("Dino-Pack: here, take this.")
            input()
            print("---Chat has ended---")
            input()
            luckyRoll = random.randint(10, 20)
            if luckCheck >= luckyRoll:
              print("You got a Lead Blade!")
              power += 1
            print("You got an HP Restore!")
            playerHP = maximumHP
            input()
            print("You are now at %s total HP!" % playerHP)
            lieDecide = "false"
        else:
          print("%s: ." % playerName)
          input()
          print("%s: .." % playerName)
          input()
          print("%s: ..." % playerName)
          input()
          if deathCount >= 1:
            print("%s: ok i killed them!" % playerName)
            input()
            print("Dino-Pack: wat")
            input()
            print("%s: heh just kidding" % playerName)
            input()
          print("Dino-Pack: ...")
          lieDecide = "false"
      elif lieOrTruth.lower() == "truth":
        if luckCheck >= checker:
          luckRoll = random.randint(1, 2)
          if luckRoll == 1:
            print("%s: uh well i think i saw them" % playerName)
            input()
            print("Dino-Pack: uh, what?")
            input()
            print("Dino-Pack: are you sure?")
            input()
            print("%s: yeah sure" % playerName)
            input()
            print("Dino-Pack: alright then...")
            lieDecide = "false"
          elif luckRoll == 2:
            print("%s: yeah, Tear went to the left, and Murdle to the right" % playerName)
            input()
            print("Dino-Pack: ah, okay")
            input()
            print("Dino-Pack: here, take this.")
            input()
            print("---Chat has ended---")
            input()
            luckyRoll = random.randint(10, 20)
            if luckCheck >= luckyRoll:
              print("You got a Lead Blade!")
              power += 1
            print("You got an HP Restore!")
            playerHP = maximumHP
            input()
            print("You are now at %s total HP!" % playerHP)
            lieDecide = "false"
        else:
          print("%s: ." % playerName)
          input()
          print("%s: .." % playerName)
          input()
          print("%s: ..." % playerName)
          input()
          if deathCount >= 1:
            print("%s: ok i killed them!" % playerName)
            input()
            print("Dino-Pack: wat")
            input()
            print("%s: heh just kidding" % playerName)
            input()
          print("Dino-Pack: ...")
          lieDecide = "false"
      else:
        print("That's not an option, please type either 'lie' or 'truth'")
        input()
    input()
    print("---Spare, or kill?---")
    print("")
    if firstKill == "true":
      print("In this game, you have the choice to either be a murderer, or run the dungeon as normal. Most of the time, you only get one chance to kill your party members, so choose wisely.")
      firstKill = "false"
    input()
    killChoice = "true"
    while killChoice == "true":
      killDino = input("Will you kill Dino-Pack? Please enter 'yes' or 'no': ")
      if killDino.lower() == "no":
        print("You did not kill Dino-Pack")
        print("")
        print("Trust was raised by 10!")
        dinoState = "spared"
        trust += 10
        killChoice = "false"
      elif killDino.lower() == "yes":
        print("You walk up to Dino-Pack, with intent to kill.")
        input()
        print("---Chat---")
        print("")
        print("Dino-Pack: ???")
        input()
        print("%s: :)" % playerName)
        input()
        print("Dino-Pack: are you trying to fight me?")
        input()
        print("Dino-Pack: we'll see about that")
        print("")
        print("---Chat has ended---")
        input()
        print("---Battle!---")
        input()
        battle = "true"
        playerHealth = HPRemaining
        playerHealing = 4
        monsterHP = 14
        monsterDamage = 4
        monsterHealing = 2
        maxPlayerHealth = playerHealth
        print("Dino-Pack initiated a battle!")
        print("")
        while playerHealth > 0:
          while battle == "true":
            turn = input("What do you do? 1-Attack, 2-Defend, 3-Heal")
            if turn == "1":
              monsterHP = monsterHP - power
              print("You dealt", power, "damage to Dino-Pack")
              if defense > monsterDamage:
                damage = 0
              else:
                damage = monsterDamage - defense
              playerHealth = playerHealth - damage
              print("")
              print("Dino-Pack dealt", damage, "damage to you!")
            elif turn == "2":
              defended = defense + 5
              if defended > monsterDamage:
                damage = 0
              else:
                damage = monsterDamage - defended
              print("You defended!")
              print("")
              print("Dino-Pack dealt", damage, "damage to you!")
            elif turn == "3":
              healER = random.randint(1, 4)
              if healER > 1:
                if playerHealth + playerHealing > maxPlayerHealth:
                  playerHealth = maxPlayerHealth
                  print("You healed yourself to max HP!")
                else:
                  playerHealth = playerHealth + playerHealing
                print("You healed yourself for", playerHealing, "HP!")
              else:
                print("Your heal failed.")
              if defense > monsterDamage:
                damage = 0
              else:
                damage = monsterDamage - defense
              playerHealth = playerHealth - damage
              print("")
              print("Dino-Pack dealt", damage, "damage to you!")
            else:
              print("That's not a valid option?")
            print("")
            print("You have", playerHealth, "HP remaining!")
            print("")
            print("Dino-Pack has", monsterHP, "HP remaining!")
            if playerHealth <= 0:
              HPRemaining = 0
              battle = "false"
            if monsterHP <= 0:
              print("You defeated Dino-Pack!")
              battle = "false"
            else:
              continue
          HPRemaining = playerHealth
          playerHealth = 0
        if HPRemaining <= 0:
          game = "false"
          gameOver = "true"
        playerHealth = HPRemaining
        print("The battle ends and you have", playerHealth, "HP remaining!")
        input()
        print("---Chat---")
        print()
        print("Dino-Pack: why")
        input()
        print("Dino-Pack: just why")
        input()
        print("%s: ..." % playerName)
        print("")
        message = input("Why do you want to kill Dino-Pack? ")
        print("")
        print(playerName, ":", message)
        input()
        print("Dino-Pack: D:")
        input()
        print("%s: any last words?" % playerName)
        input()
        print("Dino-Pack: i shouldnt have been so trusting")
        input()
        print("---Chat has ended---")
        input()
        print("You stab Dino-Pack right in the chest. As he lays dead on the floor, you feel no remorse.")
        input()
        dinoState = "dead"
        killPoints += 5
        killChoice = "false"
        deathCount += 1
    if deathCount == 3:
      print("'There's only one person left. Eevee.' you think to yourself. 'Kill her, and I can get out without penalty.'")
      input()
    else:
      print("You've visited everyone but one person, Eevee005.")
    if dinoState == "dead":
      print("You assume Eevee005 went to fight Mr. Dye, or at least went near the boss door.")
      input()
    else:
      print("After asking Dino-Pack, you learn that Eevee005 went to fight Mr. Dye by herself.")
      input()
    print("You should go find her.")
    input()
    roomChoice = "true"
    while roomChoice == "true":
      roomdecide = input("Where do you go? Forwards or back? ")
      if roomdecide.lower() == "forwards":
        roomNumber = 9
        previousRoom = 7
        roomChoice = "false"
      elif roomdecide.lower() == "back":
        roomNumber = 6
        previousRoom = 7
        roomChoice = "false"
      else:
        print("That's not a valid option.")
        print("")
  elif roomNumber == 7:
    if dinoState == "spared":
      print("Dino-Pack is sharpening his blade, most likely preparing for the fight against Mr. Dye.")
      input()
      print("You probably shouldn't bother a man with a sharp sword in his hand.")
      input()
    elif dinoState == "dead":
      print("You see Dino-Pack's body on the floor where you met him. His sword looks dull.")
      input()
      print("You should go find Eevee005.")
      input()
    roomdecide = input("Where do you go? Forwards or back? ")
    if roomdecide.lower() == "forwards":
      roomNumber = 9
      previousRoom = 7
      roomChoice = "false"
    elif roomdecide.lower() == "back":
      roomNumber = 6
      previousRoom = 7
      roomChoice = "false"
    else:
      print("That's not a valid option.")
      print("")
  elif roomNumber == 9:
    print("'Why are there so many hallways?' you ask yourself.")
    input()
    print("You spot a skull on the ground.")
    input()
    print("You then notice the rest of the defeated skeleton.")
    input()
    print("'Whew, just a dead monster. I thought it was Eevee for a second.' you say aloud, to nobody in particular.")
    input()
    if previousRoom == 7:
      roomNumber = 10
      previousRoom = 9
    elif previousRoom == 10:
      roomNumber = 7
      previousRoom = 9
  elif roomNumber == 10:
    print("Another hallway, how predictable.")
    if roomSecret == "lost":
      input()
      print("...")
      input()
      print("Something feels wrong here.")
      input()
      print("I may just be the narrator, but I think you should looks around more.")
      input()
      print("You can do a check for any hidden items.")
      input()
      checkChoice = "true"
      while checkChoice == "true":
        check = input("Would you like to check for anything hidden? ")
        if check.lower() == "yes":
          checkroll = random.randint(1, 20)
          if checkroll >= 10:
            print(
                "You don't find anything, but I swear there has to be something there...")
            roomSecret = "gone"
            checkChoice = "false"
          elif checkroll <= 10:
            print("You found a secret room! I knew there had to be something.")
            roomSecret = "found"
            checkChoice = "false"
    input()
    print("'Onwards!' you think to yourself.")
    input()
    print("WARNING! Ahead of you is the last room in the game. If you wish to finish other things first, go back. If you're ready to finish the game, move forwards.")
    roomChoice = "true"
    while roomChoice == "true":
      if roomSecret == "found":
        roomdecide = input("Where do you go? Forwards, left or back? ")
        if roomdecide.lower() == "forwards":
          print("Are you sure? You can't go back, so choose wisely.")
          input()
          finalMove = "true"
          while finalMove == "true":
            gameEnder = input("Would you actually like to continue? ")
            if gameEnder.lower() == "yes":
              roomNumber = "boss"
              previousRoom = 10
              finalMove = "false"
              roomChoice = "false"
            elif gameEnder.lower() == "no":
              print("The door will be here when you're ready.")
              finalMove = "false"
        elif roomdecide.lower() == "back":
          roomNumber = 9
          previousRoom = 10
          roomChoice = "false"
        elif roomdecide.lower() == "left":
          roomNumber = "secret"
          previousRoom = 10
          roomChoice = "false"
        else:
          print("That's not a valid option.")
          print("")
      else:
        roomdecide = input("Where do you go? Forwards or back? ")
        if roomdecide.lower() == "forwards":
          print("Are you sure? You can't go back, so choose wisely.")
          input()
          finalMove = "true"
          while finalMove == "true":
            gameEnder = input("Would you actually like to continue? ")
            if gameEnder.lower() == "yes":
              roomNumber = "boss"
              previousRoom = 10
              finalMove = "false"
              roomChoice = "false"
            elif gameEnder.lower() == "no":
              print("The door will be here when you're ready.")
              finalMove = "false"
        elif roomdecide.lower() == "back":
          roomNumber = 9
          previousRoom = 10
          roomChoice = "false"
        else:
          print("That's not a valid option.")
          print("")
  elif roomNumber == "secret":
    print("You walk into the small secret passageway.")
    input()
    print("There is a chest.")
    input()
    if roomSecretChest == "unlooted":
      print("It's un-opened.")
      input()
      chestYN = "true"
      while chestYN == "true":
        chestChoice = input("Would you like to open the chest?")
        if chestChoice.lower() == "yes":
          print("As you open the chest, you are in awe of all the riches it contains.")
          input()
          print("You got a Ruby Suit!")
          defense += 2
          input()
          print("You got a Gem Shortsword!")
          power += 1
          input()
          print("You found an HP Restore!")
          playerHP = maximumHP
          input()
          print("You are now at %s total HP!" % playerHP)
          input()
          print("You found a stat boost!")
          input()
          loot = random.randint(1, 8)
          if loot == 1:
            print("You got a small HP Boost!")
            extraPlayerHP += 3
            maximumHP += 3
          elif loot == 2:
            print("You got a small Power Boost!")
            power += 1
          elif loot == 3:
            print("You got a small Defense Boost!")
            defense += 2
          elif loot == 4:
            print("You got a small Luck Boost!")
            luck += 1
          elif loot == 5:
            print("You got a medium HP Boost!")
            extraPlayerHP += 5
            maximumHP += 5
          elif loot == 6:
            print("You got a medium Power Boost!")
            power += 2
          elif loot == 7:
            print("You got a medium Defense Boost!")
            defense += 4
          elif loot == 8:
            print("You got a medium Luck Boost!")
            luck += 2
          input()
          roomSecretChest = "looted"
          chestYN = "no"
        elif chestChoice.lower() == "no":
          print(
              "You decide not to open the chest. 'I can always return later,' you think to yourself.")
          input()
          chestYN = "no"
        else:
          print("That's not a valid option! The valid   choices are: 'Yes' or 'No'")
          input()
      print("You should go back now. The dungeon awaits!")
      input()
      roomNumber = 10
      previousRoom = "secret"
  elif roomNumber == "boss":
    print("You open the massive door, and walk in.")
    input()
    print("The room is massive, and the only thing you see is the door to leave the dungeon.")
    input()
    print("---Chat---")
    print("")
    print("%s: eevee?" % playerName)
    input()
    print("%s: you in here?" % playerName)
    input()
    print("Eevee005: %s! hi!" % playerName)
    input()
    print("Eevee005: where are the others?")
    input()
    print("%s: still in the dungeon, where's mr dye?" % playerName)
    input()
    print("Eevee005: dead, i just finished the fight")
    input()
    print("%s: by yourself?" % playerName)
    input()
    print("Eevee005: impressive, right?")
    input()
    print("%s: certainly" % playerName)
    input()
    print("Eevee005: lets wait for the others to arrive")
    input()
    print("%s: alright" % playerName)
    print("")
    print("---Chat has ended---")
    input()
    print("And so you wait.")
    input()
    print("You and Eevee005 wait, and wait, and wait some more.")
    input()
    print("---Chat---")
    print("")
    print("Eevee005: where are they???")
    input()
    print("%s: ..." % playerName)
    input()
    print("Eevee005: %s, you know something, dont you?" % playerName)
    input()
    print("Eevee005: where. are. they.")
    input()
    print("----Luck Test----")
    lieDecide = "true"
    while lieDecide == "true":
      checker = random.randint(0, 20)
      luckCheck = luck + trust + killPoints
      lieOrTruth = input("Would you like to lie, or tell the truth? ")
      if lieOrTruth.lower() == "lie":
        if luckCheck >= checker:
          luckRoll = random.randint(1, 2)
          if luckRoll == 1:
            print("%s: uh well i saw them" % playerName)
            input()
            print("Eevee: where?")
            input()
            print("%s: just" % playerName)
            input()
            print("%s: around" % playerName)
            input()
            print("Eevee005: ... i dont trust you")
            lieDecide = "false"
          elif luckRoll == 2:
            print("%s: no, i didnt see them" % playerName)
            input()
            print("Eevee005: okay?")
            input()
            print("Eevee005: its weird that theyre not here tho")
            input()
            print("---Chat has ended---")
            input()
            lieDecide = "false"
        else:
          print("%s: ." % playerName)
          input()
          print("%s: .." % playerName)
          input()
          print("%s: ..." % playerName)
          input()
          if deathCount >= 1:
            print("%s: ok i killed them!" % playerName)
            input()
            print("Eevee005: you WHAT!?")
            input()
            print("%s: heh just kidding" % playerName)
            input()
            print("Eevee005: nononono you cant weasel out of this one")
            print("---Chat has ended---")
            input()
            print("---Battle!---")
            input()
            battle = "true"
            playerHealth = HPRemaining
            playerHealing = 4
            monsterHP = 15
            monsterDamage = 5
            monsterHealing = 2
            maxPlayerHealth = playerHealth
            print("Eevee005 initiated a battle!")
            print("")
            while playerHealth > 0:
              while battle == "true":
                turn = input("What do you do? 1-Attack, 2-Defend, 3-Heal")
                if turn == "1":
                  monsterHP = monsterHP - power
                  print("You dealt", power, "damage to Eevee005!")
                  if defense > monsterDamage:
                    damage = 0
                  else:
                    damage = monsterDamage - defense
                  playerHealth = playerHealth - damage
                  print("")
                  print("Eevee005 dealt", damage, "damage to you!")
                elif turn == "2":
                  defended = defense + 5
                  if defended > monsterDamage:
                    damage = 0
                  else:
                    damage = monsterDamage - defended
                  print("You defended!")
                  print("")
                  print("Eevee005 dealt", damage, "damage to you!")
                elif turn == "3":
                  healER = random.randint(1, 4)
                  if healER > 1:
                    if playerHealth + playerHealing > maxPlayerHealth:
                      playerHealth = maxPlayerHealth
                      print("You healed yourself to max HP!")
                    else:
                      playerHealth = playerHealth + playerHealing
                    print("You healed yourself for", playerHealing, "HP!")
                  else:
                    print("Your heal failed.")
                  if defense > monsterDamage:
                    damage = 0
                  else:
                    damage = monsterDamage - defense
                  playerHealth = playerHealth - damage
                  print("")
                  print("Eevee005 dealt", damage, "damage to you!")
                else:
                  print("That's not a valid option?")
                print("")
                print("You have", playerHealth, "HP remaining!")
                print("")
                print("Eevee005 has", monsterHP, "HP remaining!")
                if playerHealth <= 0:
                  HPRemaining = 0
                  battle = "false"
                if monsterHP <= 0:
                  print("You defeated Eevee005!")
                  battle = "false"
                else:
                  continue
              HPRemaining = playerHealth
              playerHealth = 0
            if HPRemaining <= 0:
              game = "false"
              gameOver = "true"
            playerHealth = HPRemaining
            print("The battle ends and you have",
                  playerHealth, "HP remaining!")
            input()
            print("---Chat---")
            print()
            print("Eevee005: thats what i get for attackking a murderer")
            input()
            print("Eevee005: dont bother saying anything %s" % playerName)
            input()
            print("%s: but" % playerName)
            input()
            print("Eevee005: no, shush")
            input()
            print("Eevee005: let me die in peace")
            input()
            print("---Chat has ended---")
            input()
            print("You watch as the life fades from Eevee005's eyes.")
            input()
            eeveeState = "dead"
            killPoints += 5
            deathCount += 1
            lieDecide = "false"
          else:
            print("%s: i dont know where they are" % playerName)
            input()
            print("Eevee005: nononono you cant weasel out of this one")
            print("---Chat has ended---")
            input()
            print("---Battle!---")
            input()
            battle = "true"
            playerHealth = HPRemaining
            playerHealing = 4
            monsterHP = 15
            monsterDamage = 5
            monsterHealing = 2
            maxPlayerHealth = playerHealth
            print("Eevee005 initiated a battle!")
            print("")
            while playerHealth > 0:
              while battle == "true":
                turn = input("What do you do? 1-Attack, 2-Defend, 3-Heal")
                if turn == "1":
                  monsterHP = monsterHP - power
                  print("You dealt", power, "damage to Eevee005!")
                  if defense > monsterDamage:
                    damage = 0
                  else:
                    damage = monsterDamage - defense
                  playerHealth = playerHealth - damage
                  print("")
                  print("Eevee005 dealt", damage, "damage to you!")
                elif turn == "2":
                  defended = defense + 5
                  if defended > monsterDamage:
                    damage = 0
                  else:
                    damage = monsterDamage - defended
                  print("You defended!")
                  print("")
                  print("Eevee005 dealt", damage, "damage to you!")
                elif turn == "3":
                  healER = random.randint(1, 4)
                  if healER > 1:
                    if playerHealth + playerHealing > maxPlayerHealth:
                      playerHealth = maxPlayerHealth
                      print("You healed yourself to max HP!")
                    else:
                      playerHealth = playerHealth + playerHealing
                    print("You healed yourself for", playerHealing, "HP!")
                  else:
                    print("Your heal failed.")
                  if defense > monsterDamage:
                    damage = 0
                  else:
                    damage = monsterDamage - defense
                  playerHealth = playerHealth - damage
                  print("")
                  print("Eevee005 dealt", damage, "damage to you!")
                else:
                  print("That's not a valid option?")
                print("")
                print("You have", playerHealth, "HP remaining!")
                print("")
                print("Eevee005 has", monsterHP, "HP remaining!")
                if playerHealth <= 0:
                  HPRemaining = 0
                  battle = "false"
                if monsterHP <= 0:
                  print("You defeated Eevee005!")
                  battle = "false"
                else:
                  continue
              HPRemaining = playerHealth
              playerHealth = 0
            if HPRemaining <= 0:
              roomNumber = "death"
              game = "false"
              gameOver = "true"
              lieDecide = "false"
            else:
              playerHealth = HPRemaining
              print("The battle ends and you have",
                    playerHealth, "HP remaining!")
              input()
              print("---Chat---")
              print()
              print("Eevee005: thats what i get for attackking an innocent")
              input()
              print("Eevee005: dont bother saying anything %s" % playerName)
              input()
              print("%s: but" % playerName)
              input()
              print("Eevee005: no, shush")
              input()
              print("Eevee005: let me die in peace")
              input()
              print("---Chat has ended---")
              input()
              print("You watch as the life fades from Eevee005's eyes.")
              input()
              eeveeState = "dead"
              killPoints += 5
              deathCount += 1
              lieDecide = "false"
      elif lieOrTruth.lower() == "truth":
        if luckCheck >= checker:
          luckRoll = random.randint(1, 2)
          if luckRoll == 1:
            print("%s: uh well i saw them" % playerName)
            input()
            print("Eevee: where?")
            input()
            print("%s: just" % playerName)
            input()
            print("%s: around" % playerName)
            input()
            print("Eevee005: ... i dont trust you")
            lieDecide = "false"
          elif luckRoll == 2:
            print("%s: yeah, Tear went to the left, and Murdle to the right, i met Dino about 3 rooms ago" % playerName)
            input()
            print("Eevee005: alright")
            input()
            print("Eevee005: we should go look for them")
            input()
            print("---Chat has ended---")
            input()
            lieDecide = "false"
        else:
          print("%s: ." % playerName)
          input()
          print("%s: .." % playerName)
          input()
          print("%s: ..." % playerName)
          input()
          if deathCount >= 1:
            print("%s: ok i killed them!" % playerName)
            input()
            print("Eevee005: you WHAT!?")
            input()
            print("%s: heh just kidding" % playerName)
            input()
            print("Eevee005: nononono you cant weasel out of this one")
            input()
            print("---Chat has ended---")
            input()
            print("---Battle!---")
            input()
            battle = "true"
            playerHealth = HPRemaining
            playerHealing = 4
            monsterHP = 15
            monsterDamage = 5
            monsterHealing = 2
            maxPlayerHealth = playerHealth
            print("Eevee005 initiated a battle!")
            print("")
            while playerHealth > 0:
              while battle == "true":
                turn = input("What do you do? 1-Attack, 2-Defend, 3-Heal")
                if turn == "1":
                  monsterHP = monsterHP - power
                  print("You dealt", power, "damage to Eevee005!")
                  if defense > monsterDamage:
                    damage = 0
                  else:
                    damage = monsterDamage - defense
                  playerHealth = playerHealth - damage
                  print("")
                  print("Eevee005 dealt", damage, "damage to you!")
                elif turn == "2":
                  defended = defense + 5
                  if defended > monsterDamage:
                    damage = 0
                  else:
                    damage = monsterDamage - defended
                  print("You defended!")
                  print("")
                  print("Eevee005 dealt", damage, "damage to you!")
                elif turn == "3":
                  healER = random.randint(1, 4)
                  if healER > 1:
                    if playerHealth + playerHealing > maxPlayerHealth:
                      playerHealth = maxPlayerHealth
                      print("You healed yourself to max HP!")
                    else:
                      playerHealth = playerHealth + playerHealing
                    print("You healed yourself for", playerHealing, "HP!")
                  else:
                    print("Your heal failed.")
                  if defense > monsterDamage:
                    damage = 0
                  else:
                    damage = monsterDamage - defense
                  playerHealth = playerHealth - damage
                  print("")
                  print("Eevee005 dealt", damage, "damage to you!")
                else:
                  print("That's not a valid option?")
                print("")
                print("You have", playerHealth, "HP remaining!")
                print("")
                print("Eevee005 has", monsterHP, "HP remaining!")
                if playerHealth <= 0:
                  HPRemaining = 0
                  battle = "false"
                if monsterHP <= 0:
                  print("You defeated Eevee005!")
                  battle = "false"
                else:
                  continue
              HPRemaining = playerHealth
              playerHealth = 0
            if HPRemaining <= 0:
              game = "false"
              gameOver = "true"
            playerHealth = HPRemaining
            print("The battle ends and you have",
                  playerHealth, "HP remaining!")
            input()
            print("---Chat---")
            print()
            print("Eevee005: thats what i get for attackking a murderer")
            input()
            print("Eevee005: dont bother saying anything %s" % playerName)
            input()
            print("%s: but" % playerName)
            input()
            print("Eevee005: no, shush")
            input()
            print("Eevee005: let me die in peace")
            input()
            print("---Chat has ended---")
            input()
            print("You watch as the life fades from Eevee005's eyes.")
            input()
            eeveeState = "dead"
            killPoints += 5
            deathCount += 1
            lieDecide = "false"
      else:
        print("That's not an option, please type either 'lie' or 'truth'")
        input()
    if eeveeState == "dead":
      print("You stand there, confused as to what just happened.")
      input()
      if deathCount >= 3:
        print("You oddly feel no remorse, or sadness.")
        input()
      else:
        print("Saddened by the events that just unfolded, you turn around.")
        input()
        print("You can't stand yourself.")
        input()
    if eeveeState == "alive":
      if deathCount >= 3:
        print("You know what you must do.")
      input()
      print("---Spare or Kill?---")
      print("")
      if firstKill == "true":
        print("In this game, you have the choice to either be a murderer, or run the dungeon as normal. Most of the time, you only get one chance to kill your party members, so choose wisely.")
        firstKill = "false"
      input()
      killChoice = "true"
      while killChoice == "true":
        killEevee = input(
            "Will you kill Eevee005? Please enter 'yes' or 'no': ")
        if killEevee.lower() == "no":
          print("You did not kill Eevee005.")
          print("")
          print("Trust was raised by 10!")
          eeveeState = "spared"
          trust += 10
          killChoice = "false"
        elif killEevee.lower() == "yes":
          print("You strike quickly, not leaving any time for Eevee005 to speak.")
          input()
          print("---Battle!---")
          input()
          battle = "true"
          playerHealth = HPRemaining
          playerHealing = 4
          monsterHP = 15
          monsterDamage = 5
          monsterHealing = 2
          maxPlayerHealth = playerHealth
          print("You initiated a battle!")
          print("")
          while playerHealth > 0:
            while battle == "true":
              turn = input("What do you do? 1-Attack, 2-Defend, 3-Heal")
              if turn == "1":
                monsterHP = monsterHP - power
                print("You dealt", power, "damage to Eevee005!")
                if defense > monsterDamage:
                  damage = 0
                else:
                  damage = monsterDamage - defense
                playerHealth = playerHealth - damage
                print("")
                print("Eevee005 dealt", damage, "damage to you!")
              elif turn == "2":
                defended = defense + 5
                if defended > monsterDamage:
                  damage = 0
                else:
                  damage = monsterDamage - defended
                print("You defended!")
                print("")
                print("Eevee005 dealt", damage, "damage to you!")
              elif turn == "3":
                healER = random.randint(1, 4)
                if healER > 1:
                  if playerHealth + playerHealing > maxPlayerHealth:
                      playerHealth = maxPlayerHealth
                      print("You healed yourself to max HP!")
                  else:
                    playerHealth = playerHealth + playerHealing
                  print("You healed yourself for", playerHealing, "HP!")
                else:
                  print("Your heal failed.")
                if defense > monsterDamage:
                  damage = 0
                else:
                  damage = monsterDamage - defense
                playerHealth = playerHealth - damage
                print("")
                print("Eevee005 dealt", damage, "damage to you!")
              else:
                print("That's not a valid option?")
              print("")
              print("You have", playerHealth, "HP remaining!")
              print("")
              print("Eevee005 has", monsterHP, "HP remaining!")
              if playerHealth <= 0:
                HPRemaining = 0
                battle = "false"
              if monsterHP <= 0:
                print("You defeated Eevee005!")
                battle = "false"
              else:
                continue
            HPRemaining = playerHealth
            playerHealth = 0
          if HPRemaining <= 0:
            game = "false"
            gameOver = "true"
          else:
            playerHealth = HPRemaining
            print("The battle ends and you have",
                  playerHealth, "HP remaining!")
            input()
            print("---Chat---")
            print()
            print("Eevee005: thats what i get for waiting with a murderer")
            input()
            print("Eevee005: dont bother saying anything %s" % playerName)
            input()
            print("%s: but" % playerName)
            input()
            print("Eevee005: no, shush")
            input()
            print("Eevee005: let me die in peace")
            input()
            print("---Chat has ended---")
            input()
            print("You watch as the life fades from Eevee005's eyes.")
            input()
            eeveeState = "dead"
            killPoints += 5
            deathCount += 1
            killChoice = "false"
    if murdleState == "dead" and tearState == "dead" and dinoState == "dead":
      if eeveeState == "dead":
        print("Everyone is dead. You're the only one left.")
        input()
        print("There's only one thing left to do.")
        input()
        print("Run from the dungeon.")
        input()
        print("You walk up to the door, and open it. The white light blinds you, but you walk forwards anyway.")
        input()
        if tearState == "alive":
          tearState = "spared"
        if murdleState == "alive":
          murdleState = "spared"
        if dinoState == "alive":
          dinoState = "spared"
        if eeveeState == "alive":
          eeveeState = "spared"
        epilogue = "true"
        game = "false"
      else:
        print("Just two left.")
        input()
        print("You and Eevee005.")
        input()
        print(
            "So you wait, and wait. You're leading her on, but you don't know what to do.")
        input()
        print("---Chat---")
        input()
        print("Eevee005: im going to find the others")
        input()
        print("Eevee005: theyre taking too long")
        input()
        print("%s: uh okay..." % playerName)
        input()
        print("---Chat has ended---")
        input()
        print("You watch as she walks out of the room. You could follow her, or stay where you are.")
        input()
        YorN = "true"
        while YorN == "true":
          yeOrNo = input("Do you want to follow Eevee005? ")
          if yeOrNo.lower() == "yes":
            print("You decide to fol-")
            input()
            YorN = "false"
          elif yeOrNo.lower() == "no":
            print("You decide to wa-")
            input()
            YorN = "false"
          else:
            print("That's not a va-")
            input()
            YorN = "false"
        print("Eevee runs back into the room.")
        input()
        print("---Chat---")
        input()
        print("Eevee005: its dino!")
        input()
        print("Eevee005: he's dead!")
        input()
        print("%s: what?" % playerName)
        input()
        print("Eevee005: i think he was killed by a monster")
        input()
        print("%s: probably" % playerName)
        input()
        print("Eevee005: the others are most likely dead too...")
        input()
        print("%s: i wonder what killed them" % playerName)
        input()
        print("Eevee005: i dont know")
        input()
        print("Eevee005: lets not waste time and leave the dungeon.")
        input()
        print("%s: alright" % playerName)
        input()
        print("---Chat has ended---")
        input()
        print("You and Eevee005 walk up to the door, and Eevee005 opens it up. The light is blinding, but Eevee005 gets you to walk forwards.")
        input()
        if tearState == "alive":
          tearState = "spared"
        if murdleState == "alive":
          murdleState = "spared"
        if dinoState == "alive":
          dinoState = "spared"
        if eeveeState == "alive":
          eeveeState = "spared"
        epilogue = "true"
        game = "false"
    else:
      print("You notice everyone is starting to file in.")
      input()
      if dinoState == "spared" or "alive":
        print("Dino-Pack walks in.")
      if murdleState == "spared" or "alive":
        print("MurdleMuffin walks in.")
      if tearState == "spared" or "alive":
        print("Tear 2bad walks in.")
      if eeveeState == "dead":
        print("You realise that everyone is staring at Eevee005's dead body at your feet.")
        input()
        print("---Chat---")
        input()
        print("%s: its not what it looks like" % playerName)
        input()
        print("%s: i swear" % playerName)
        input()
        if dinoState == "dead":
          if tearState == "dead":
            print("MurdleMuffin: i think we can tell exactly what happened")
          else:
            print("Tear 2bad: i think we can tell exactly what happened")
        else:
          print("Dino-Pack: i think we can tell exactly what happened")
        input()
        print("%s: no please" % playerName)
        input()
        print("%s: believe me i didnt kill her" % playerName)
        if murdleState == "dead":
          if dinoState == "dead":
            print("Tear 2bad: then who did?")
          else:
            print("Dino-Pack: then who did?")
        else:
          print("MurdleMuffin: then who did?")
        input()
        print("%s: it was mr dye" % playerName)
        input()
        if tearState == "dead":
          if murdleState == "dead":
            print("Dino-Pack: so you saw mr dye kill eevee, and then killed mr dye?")
          else:
            print("MurdleMuffin: so you saw mr dye kill eevee, and then killed mr dye?")
        else:
          print("Tear 2bad: so you saw mr dye kill eevee, and then killed mr dye?")
        input()
        print("%s: yes exactly" % playerName)
        input()
        if dinoState == "spared" or "alive":
          print("Dino-Pack: hmmm")
          input()
        if murdleState == "spared" or "alive":
          print("MurdleMuffin: ._.")
          input()
        if tearState == "spared" or "alive":
          print("Tear 2bad: i dont trust you")
          input()
        print("%s: theres no point in fighting" % playerName)
        input()
        print("%s: lets just leave" % playerName)
        input()
        if dinoState == "dead":
          if tearState == "dead":
            print("MurdleMuffin: alright")
          else:
            print("Tear 2bad: alright")
        else:
          print("Dino-Pack: alright")
        input()
        print("---Chat has ended---")
        input()
        print("You walk up to the door, and open it. The bright light blinds you, but you lead the others through the door anyway.")
        if tearState == "alive":
          tearState = "spared"
        if murdleState == "alive":
          murdleState = "spared"
        if dinoState == "alive":
          dinoState = "spared"
        if eeveeState == "alive":
          eeveeState = "spared"
        epilogue = "true"
        game = "false"
      else:
        print("---Chat---")
        input()
        print("Eevee005: you're here!")
        input()
        print("%s: whew, finally" % playerName)
        input()
        print("%s: we thought you died" % playerName)
        if dinoState == "dead":
          if murdleState == "dead":
            print("Tear 2bad: no, not dead lol")
          else:
            print("MurdleMuffin: no, not dead lol")
        else:
          print("Dino-Pack: no, not dead lol")
        input()
        print("Eevee005: what took you guys so long?")
        input()
        if tearState == "alive" or "spared":
          print("Tear 2bad: i had to fight a dragon")
          input()
          print("Eevee005: ...")
          input()
          print("Tear 2bad: jk lol")
        if murdleState == "alive" or "spared":
          print("MurdleMuffin: i just forgot we were meeting here for a while")
          input()
          print("%s: fair enough" % playerName)
          input()
        if dinoState == "alive" or "spared":
          print("Dino-Pack: i went to find the others before coming here")
          input()
          print("Eevee005: aww, that's nice")
          input()
          print("Dino-Pack: :)")
          input()
        print("Eevee005: alright then, shall we go?")
        input()
        if dinoState == "spared" or "alive":
          print("Dino-Pack: we did it")
          input()
        if murdleState == "spared" or "alive":
          print("MurdleMuffin: finally were done")
          input()
        if tearState == "spared" or "alive":
          print("Tear 2bad: i wish i could kill mr dye D:")
          input()
        print("%s: lets finish this." % playerName)
        input()
        print("---Chat has ended---")
        input()
        print("Eevee005 walks up to the door, and opens it. The light blinds you, but you lead the party out of the dungeon.")
        input()
        if tearState == "alive":
          tearState = "spared"
        if murdleState == "alive":
          murdleState = "spared"
        if dinoState == "alive":
          dinoState = "spared"
        if eeveeState == "alive":
          eeveeState = "spared"
        epilogue = "true"
        game = "false"
  elif roomNumber == "tearKillRoom":
    print("You walk to the room on your left, and find Tear 2bad there, digging into a chest.")
    input()
    print("---Chat---")
    print("")
    print("%s: hi" % playerName)
    input()
    print("Tear 2bad: hello")
    input()
    print("Tear 2bad: this loot is awesome i found a nice sword")
    input()
    print("%s: thats nice" % playerName)
    input()
    print("Tear 2bad: here have this other sword i found too")
    input()
    print("%s: thanks" % playerName)
    print("")
    print("---Chat has ended---")
    input()
    print("You got a Slimy Blade!")
    power += 1
    input()
    print("---Spare, or kill?---")
    print("")
    if firstKill == "true":
      print("In this game, you have the choice to either be a murderer, or run the dungeon as normal. Most of the time, you only get one chance to kill your party members, so choose wisely.")
      firstKill = "false"
    input()
    killChoice = "true"
    while killChoice == "true":
      killTear = input("Will you kill Tear 2bad? Please enter 'yes' or 'no': ")
      if killTear.lower() == "no":
        print("You did not kill Tear 2bad.")
        print("")
        print("Trust was raised by 10!")
        tearState = "spared"
        trust += 10
        killChoice = "false"
      elif killTear.lower() == "yes":
        print("You walk up to Tear 2bad, with your new Slimy Blade in hand, and land a blow on his head.")
        input()
        print("---Chat---")
        print("")
        print("Tear 2bad: %s what are you doing, im not an enemy" % playerName)
        input()
        print("%s: i know :)" % playerName)
        input()
        print("Tear 2bad: what")
        input()
        print("Tear 2bad: are u trying to kill me?")
        input()
        print("%s: yes :))" % playerName)
        input()
        print("Tear 2bad: you cant beat me")
        print("")
        print("---Chat has ended---")
        input()
        print("---Battle!---")
        input()
        battle = "true"
        playerHealth = HPRemaining
        playerHealing = 4
        monsterHP = 11
        monsterDamage = 4
        monsterHealing = 2
        maxPlayerHealth = playerHealth
        print("Tear 2bad initiated a battle!")
        print("")
        while playerHealth > 0:
          while battle == "true":
            turn = input("What do you do? 1-Attack, 2-Defend, 3-Heal")
            if turn == "1":
              monsterHP = monsterHP - power
              print("You dealt", power, "damage to Tear 2bad!")
              if defense > monsterDamage:
                damage = 0
              else:
                damage = monsterDamage - defense
              playerHealth = playerHealth - damage
              print("")
              print("Tear 2bad dealt", damage, "damage to you!")
            elif turn == "2":
              defended = defense + 5
              if defended > monsterDamage:
                damage = 0
              else:
                damage = monsterDamage - defended
              print("You defended!")
              print("")
              print("Tear 2bad dealt", damage, "damage to you!")
            elif turn == "3":
              healER = random.randint(1, 4)
              if healER > 1:
                if playerHealth + playerHealing > maxPlayerHealth:
                  playerHealth = maxPlayerHealth
                  print("You healed yourself to max HP!")
                else:
                  playerHealth = playerHealth + playerHealing
                print("You healed yourself for", playerHealing, "HP!")
              else:
                print("Your heal failed.")
              if defense > monsterDamage:
                damage = 0
              else:
                damage = monsterDamage - defense
              playerHealth = playerHealth - damage
              print("")
              print("Tear 2bad dealt", damage, "damage to you!")
            else:
              print("That's not a valid option?")
            print("")
            print("You have", playerHealth, "HP remaining!")
            print("")
            print("Tear 2bad has", monsterHP, "HP remaining!")
            if playerHealth <= 0:
              HPRemaining = 0
              battle = "false"
            if monsterHP <= 0:
              print("You defeated Tear 2bad!")
              battle = "false"
            else:
              continue
          HPRemaining = playerHealth
          playerHealth = 0
        if HPRemaining <= 0:
          game = "false"
          gameOver = "true"
        playerHealth = HPRemaining
        print("The battle ends and you have", playerHealth, "HP remaining!")
        input()
        print("---Chat---")
        print()
        print("Tear 2bad: you win")
        input()
        print("Tear 2bad: why are you doing this %s" % playerName)
        input()
        print("%s: i have my reasons" % playerName)
        input()
        print("Tear 2bad: and what are those?")
        print("")
        message = input("Why do you want to kill Tear 2bad?")
        print("")
        print(playerName, ":", message)
        input()
        print("Tear 2bad: you're evil")
        input()
        print("%s: any last words?" % playerName)
        input()
        print("Tear 2bad: i knew you sucked the moment i met you")
        input()
        print("---Chat has ended---")
        input()
        print(
            "You raise your sword above Tear 2bad's head, and swing down, killing Tear 2bad.")
        input()
        tearState = "dead"
        killPoints += 5
        deathCount += 1
        killChoice = "false"
    if tearState == "dead":
      print("You stand there, unsure of what to do.")
      input()
      print("You decide that staying in the room is a bad idea.")
      input()
    elif tearState == "spared":
      print("Walking away, you decide where you will go.")
      input()
    print("Standing in the middle of the room, you notice there's a door to your right, your left, and back the way you came.")
    input()
    roomChoice = "true"
    while roomChoice == "true":
      roomdecide = input("Where do you go? Right, left or back? ")
      if roomdecide.lower() == "left":
        roomNumber = 12
        previousRoom = 11
        roomChoice = "false"
      elif roomdecide.lower() == "right":
        roomNumber = 13
        previousRoom = 11
        roomChoice = "false"
      elif roomdecide.lower() == "back":
        roomNumber = 1
        previousRoom = 11
        roomChoice = "false"
      else:
        print("That's not a valid option.")
        print("")
  elif roomNumber == 12:
    print("You walk into the room to your left.")
    input()
    print("It's an empty room, except for some enemy remains.")
    input()
    print("'Tear's probably been in here.' You think to yourself. 'Looks like a fight broke out.'")
    input()
    print("Nothing else in the room, you decide to turn back around.")
    input()
    previousRoom = 12
    roomNumber = 11
  elif roomNumber == 11:
    print("You walk into the room.")
    input()
    if tearState == "dead":
      print("You notice Tear 2bad's corpse still in the room.")
      input()
      print("You shudder.")
      input()
    elif tearState == "spared":
      print("You see Tear 2bad looking around at a wall, seemingly pensive.")
      input()
      print("You decide not to question it.")
      input()
    print("Standing in the middle of the room, you notice there's a door to your right, your left, and back the way you came.")
    input()
    roomChoice = "true"
    while roomChoice == "true":
      roomdecide = input("Where do you go? Right, left or back? ")
      if roomdecide.lower() == "left":
        roomNumber = 12
        previousRoom = 11
        roomChoice = "false"
      elif roomdecide.lower() == "right":
        roomNumber = 13
        previousRoom = 11
        roomChoice = "false"
      elif roomdecide.lower() == "back":
        roomNumber = 1
        previousRoom = 11
        roomChoice = "false"
      else:
        print("That's not a valid option.")
        print("")
  elif roomNumber == 13:
    print("The room is just a hallway, nothing special.")
    input()
    print("You decide to move into the room in front of you.")
    input()
    if previousRoom == 11:
      previousRoom = 13
      roomNumber = 14
    elif previousRoom == 14:
      previousRoom = 13
      roomNumber = 11
    else:
      print("Something went wrong. Either the code has a mistake, or the game lost track of where you are. Setting your room to the starting room...")
      roomNumber = 1
  elif roomNumber == 14:
    if room14Monster == "alive":
      print("You walk into the room, coming out of the hallway, and find a Deadly Flower in the room!")
      input()
      print("---Battle!---")
      input()
      battle = "true"
      playerHealth = HPRemaining
      playerHealing = 4
      monsterHP = 9
      monsterDamage = 5
      monsterHealing = 0
      maxPlayerHealth = playerHealth
      print("You encountered a Deadly Flower!")
      print("")
      while playerHealth > 0:
        while battle == "true":
          turn = input("What do you do? 1-Attack, 2-Defend, 3-Heal")
          if turn == "1":
            monsterHP = monsterHP - power
            print("You dealt", power, "damage to the Deadly Flower!")
            if defense > monsterDamage:
              damage = 0
            else:
              damage = monsterDamage - defense
            playerHealth = playerHealth - damage
            print("")
            print("The Deadly Flower dealt", damage, "damage to you!")
          elif turn == "2":
            defended = defense + 5
            if defended > monsterDamage:
              damage = 0
            else:
              damage = monsterDamage - defended
            print("You defended!")
            print("")
            print("The Deadly Flower dealt", damage, "damage to you!")
          elif turn == "3":
            healER = random.randint(1, 4)
            if healER > 1:
              if playerHealth + playerHealing > maxPlayerHealth:
                playerHealth = maxPlayerHealth
                print("You healed yourself to max HP!")
              else:
                playerHealth = playerHealth + playerHealing
              print("You healed yourself for", playerHealing, "HP!")
            else:
              print("Your heal failed.")
            if defense > monsterDamage:
              damage = 0
            else:
              damage = monsterDamage - defense
            playerHealth = playerHealth - damage
            print("")
            print("The Deadly Flower dealt", damage, "damage to you!")
          else:
            print("That's not a valid option?")
          print("")
          print("You have", playerHealth, "HP remaining!")
          print("")
          print("The Deadly Flower has", monsterHP, "HP remaining!")
          if playerHealth <= 0:
            HPRemaining = 0
            battle = "false"
          if monsterHP <= 0:
            print("You defeated the Deadly Flower!")
            battle = "false"
          else:
            continue
          HPRemaining = playerHealth
          playerHealth = 0
        if HPRemaining <= 0:
          game = "false"
          gameOver = "true"
        print("The battle ends and you have", HPRemaining, "HP remaining!")
        input()
      playerHealth = HPRemaining
      print("The enemy dropped a stat boost!")
      input()
      loot = random.randint(1, 4)
      if loot == 1:
        print("You got a small HP Boost!")
        extraPlayerHP += 3
        maximumHP += 3
      elif loot == 2:
        print("You got a small Power Boost!")
        power += 1
      elif loot == 3:
        print("You got a small Defense Boost!")
        defense += 2
      elif loot == 4:
        print("You got a small Luck Boost!")
        luck += 1
      input()
      room14Monster = "dead"
    if room14Chest == "unlooted":
      print("You notice a chest at the back of the room.")
      input()
      chestYN = "true"
    while chestYN == "true":
      chestChoice = input("Would you like to open the chest?")
      if chestChoice.lower() == "yes":
        print("Walking up to it, you image all the riches it may contain.")
        input()
        print("")
        print("The chest contained a stat boost!")
        input()
        loot = random.randint(1, 8)
        if loot == 1:
          print("You got a small HP Boost!")
          extraPlayerHP += 3
          maximumHP += 3
        elif loot == 2:
          print("You got a small Power Boost!")
          power += 1
        elif loot == 3:
          print("You got a small Defense Boost!")
          defense += 2
        elif loot == 4:
          print("You got a small Luck Boost!")
          luck += 1
        elif loot == 5:
          print("You got a medium HP Boost!")
          extraPlayerHP += 5
          maximumHP += 5
        elif loot == 6:
          print("You got a medium Power Boost!")
          power += 2
        elif loot == 7:
          print("You got a medium Defense Boost!")
          defense += 4
        elif loot == 8:
          print("You got a medium Luck Boost!")
          luck += 2
        input()
        room14Chest = "looted"
        chestYN = "no"
      elif chestChoice.lower() == "no":
        print("You decide not to open the chest. 'I can always return later,' you think to yourself.")
        input()
        chestYN = "no"
      else:
        print("That's not a valid option! The valid choices are: 'Yes' or 'No'")
        input()
    print("With nothing left to do, you decide to leave back into the hallway.")
    input()
    previousRoom = 14
    roomNumber = 13
  elif roomNumber == "death":
    print("")


if epilogue == "true":
  #No deaths
  if tearState == "alive" and murdleState == "alive" and dinoState == "alive" and eeveeState == "alive":
    epilogueID = 0
  #One Death
  elif tearState == "dead" and murdleState == "alive" and dinoState == "alive" and eeveeState == "alive":
    epilogueID = 1
  elif tearState == "alive" and murdleState == "dead" and dinoState == "alive" and eeveeState == "alive":
    epilogueID = 2
  elif tearState == "alive" and murdleState == "alive" and dinoState == "dead" and eeveeState == "alive":
    epilogueID = 3
  elif tearState == "alive" and murdleState == "alive" and dinoState == "alive" and eeveeState == "dead":
    epilogueID = 4
  #Two Deaths
  elif tearState == "dead" and murdleState == "dead" and dinoState == "alive" and eeveeState == "alive":
    epilogueID = 5
  elif tearState == "dead" and murdleState == "alive" and dinoState == "dead" and eeveeState == "alive":
    epilogueID = 6
  elif tearState == "dead" and murdleState == "alive" and dinoState == "alive" and eeveeState == "dead":
    epilogueID = 7
  elif tearState == "alive" and murdleState == "dead" and dinoState == "dead" and eeveeState == "alive":
    epilogueID = 8
  elif tearState == "alive" and murdleState == "alive" and dinoState == "dead" and eeveeState == "dead":
    epilogueID = 9
  #Three Deaths
  elif tearState == "dead" and murdleState == "dead" and dinoState == "dead" and eeveeState == "alive":
    epilogueID = 10
  elif tearState == "dead" and murdleState == "dead" and dinoState == "alive" and eeveeState == "dead":
    epilogueID = 11
  elif tearState == "dead" and murdleState == "alive" and dinoState == "dead" and eeveeState == "dead":
    epilogueID = 12
  elif tearState == "alive" and murdleState == "dead" and dinoState == "dead" and eeveeState == "dead":
    epilogueID = 13
  #Four Deaths
  elif tearState == "dead" and murdleState == "dead" and dinoState == "dead" and eeveeState == "dead":
    epilogueID = 14
  #Epilogues
  if epilogueID == 0:
    print("You regain your vision and see a crowd of people cheering for you and the rest of your team.")
    input()
    print("You notice that Dino-Pack and Eevee005 are both waving to the audience, Tear 2bad is beaming, and MurdleMuffin is quite surprised, clearly not expecting or wanting this attention.")
    input()
    print("Then, you hear a voice. The same voice from the beginning of the run.")
    input()
    print("“Welcome, welcome! My name is PSugar400, and congratulations! You’ve defeated the first floor of the dungeon, and we’re all excited for you! This means that the first official play test is a success!” said PSugar400.")
    input()
    print("“Now, as the audience knows, we have a special guest! The Chosen Hero, %s, is here today with us!” the audience roars with cheers, and they stand up, waving, and thanking the crowd." % heroName)
    input()
    print("%s looks at you and your team. You notice they take a longer look at you, but are still smiling." % heroName)
    input()
    print("PSugar400 continues. “But back to our team of playtesters, I think they deserve a name, what do you reckon?” The stands agrees loudly. “How about the Fighting Team?” the audience cheers out the name.")
    input()
    print("“Fighting Team! Fighting Team! Fighting Team! Fighting Team! Fighting Team! Fighting Team!”")
    input()
    print("The Arm Attack Inc. Staff calm down the audience before PSugar400 then keeps talking. ”Now, let’s hear a word from each member from our newly named Fighting Team. %s, you’re up first." % playerName)
    input()
    message = input("What would you like to say? ")
    print("“%s” you say." % message)
    input()
    print("“Thank you for your words, %s. Tear 2bad, what would you like to say to the audience?” replied PSugar400." % playerName)
    input()
    print("“It. Was. AWESOME! I was slashing enemies, taking them down, I carried the team. Okay, well, maybe not. But still, it was great.” he answered. “Wow, what a story! So, MurdleMuffin, what do you think?” asked PSugar400.")
    input()
    print("“It was fine, I kinda just stayed in one room for a while.” he answered simply.")
    input()
    print("“Oh. Well, that’s okay. Dino-Pack, what do you have to say?” said PSugar400, continuing her interview.")
    input()
    print("“I ended up finding some really good gear. Gave some extras to other team members. Fighting enemies was really fun! I’d love to do the other floors.” he told PSugar400.")
    input()
    print("“Well, in the future, you might actually be able to do the other 2 floors!” she told Dino-Pack. The audience hollers with excitement, but PSugar400’s interview isn’t over. “And now, last but not least is Eevee005! What are your thoughts on the entire thing?”")
    input()
    print("“Much like the rest of us, I enjoyed it very much. Although If you have the ability to, the Arm Attack Inc. team should make the boss more powerful. I took down the boss by myself, and I think that the entire team, if not, at least more than one person should be required to take down Mr. Dye.” she said.")
    input()
    print("“Alright, I’ll pass that feedback on to the Development team. Thank you! And that was the Multiplayer Test for our dungeon here at Arm Attack Inc.! Thank you for being here. Once again, I’ve been your host, PSugar400, and goodnight.” finished PSugar400, walking up to you and your team.")
    input()
    print("“Tomorrow you all have meetings with %s. We’ve given you rooms in a nearby hotel. You all will have reps that you’ll work with. For now, follow the security team. They’ll lead you to your hotel rooms.” informed PSugar400." % heroName)
    input()
    print("You follow PSugar400’s lead along with the rest of your team. Eventually, PSugar400 breaks off the path, and you instead follow a security team. They bring you past crowds, so you wave to them. You notice everyone but MurdleMuffin is doing the same.")
    input()
    print("After a good 10 minutes, you arrive at your hotel room. Room 105. It’s not a very large room, but bigger than general hotel rooms. You look at the time, it’s only 3:00 PM, but you feel very tired. The bed looks very soft and comfortable. You decide to lay down, and then close your eyes, falling into a comfortable sleep.")
    input()
    print("The end.")
    input()
    print("Your game ID is %s. If you play the Floor 2 or Floor 3 games for YAAG Murder Mystery Edition, you’ll need it." % epilogueID)
    input()
    print("Thank you very much for playing my game! It was lots of fun to write, develop and all that. I’ll be making more in the future, so keep an eye out for that!")
    input()
  elif epilogueID == 1:
    print("You regain your vision and see a crowd of people cheering for you and the rest of your team.")
    input()
    print("Then, the crowd starts gasping and murmuring.")
    input()
    print("“They noticed Tear 2bad’s not here.” said Eevee005.")
    input()
    print("You hear a voice. The same voice from the beginning of the run.")
    input()
    print("“Welcome, welcome! My name is PSugar400, and congratulations! You’ve defeated the first floor of the dungeon, and we’re all excited for you! Wait, there’s only four of you?” said PSugar400.")
    input()
    print("“Uh, yeah. Tear 2bad was killed by enemies.” you say.")
    input()
    print("“Oh, I see. Let us take a moment to remember them.” said PSugar400. “I have a memorial letter written for them. I’ll read it.”")
    input()
    print("“Tear 2bad was one of the first to sign up for our multiplayer test. He was very excited to play in this game. He said he liked the fighting aspect of the test. He wanted to fight Mr. Dye as soon as possible. Let us take a moment of silence.” The crowd was silent for a minute.")
    input()
    print("“Now, as the audience knows, we have a special guest! The Chosen Hero, %s, is here today with us!” the audience roars with cheers, and %s stands up, waving, and thanking the crowd." % heroName)
    input()
    print("%s looks at you and your team. You notice they look at you suspiciously. But soon looks away." % heroName)
    input()
    print("PSugar400 continues. “But back to our team of playtesters, I think they deserve a name, what do you reckon?” The stands agrees loudly. “How about the Fighting Team?” the audience cheers out the name.")
    input()
    print("“Fighting Team! Fighting Team! Fighting Team! Fighting Team! Fighting Team! Fighting Team!”")
    input()
    print("The Arm Attack Inc. Staff calm down the audience before PSugar400 then keeps talking. ”Now, let’s hear a word from each member from our newly named Fighting Team. %s, you’re up first." % playerName)
    input()
    message = input("Type a message to tell the audience. ")
    print("“%s” you say." % message)
    input()
    print("“Thank you for your words, %s.”" % message)
    input()
    print("“So, MurdleMuffin, what do you think?” asked PSugar400.")
    input()
    print("“It was fine, I kinda just stayed in one room for a while.” he answered simply. “Oh. Well, that’s okay.”")
    input()
    print("“Dino-Pack, what do you have to say?” said PSugar400, continuing her interview.")
    input()
    print("“I ended up finding some really good gear. Gave some extras to other team members. Fighting enemies was really fun! I’d love to do the other floors.” he told PSugar400.")
    input()
    print("“Well, in the future, you might actually be able to do the other 2 floors!” she told Dino-Pack.")
    input()
    print("“And now, last but not least is Eevee005! What are your thoughts on the entire thing?”")
    input()
    print("“Much like the rest of us, I enjoyed it very much. Although If you have the ability to, the Arm Attack Inc. team should make the boss more powerful. I took down the boss by myself, and I think that the entire team, if not, at least more than one person should be required to take down Mr. Dye.” she said.")
    input()
    print("“Alright, I’ll pass that feedback on to the Development team. Thank you!” said PSugar400.")
    input()
    print("“And that was the Multiplayer Test for our dungeon here at Arm Attack Inc.! Thank you for being here. Once again, I’ve been your host, PSugar400, and goodnight.” finished PSugar400, walking up to you and your team.")
    input()
    print("“Tomorrow you all have meetings with %s. We’ve given you rooms in a nearby hotel. You all will have reps that you’ll work with. For now, follow the security team. They’ll lead you to your hotel rooms.” informed PSugar400." % heroName)
    input()
    print("You follow PSugar400’s lead along with the rest of your team. Eventually, PSugar400 breaks off the path, and you instead follow a security team. They bring you past crowds, so you wave to them.")
    input()
    print("After a good 10 minutes, you arrive at your hotel room. Room 105. It’s not a very large room, but bigger than general hotel rooms. You look at the time, it’s only 3:00 PM, but you feel very tired. The bed looks very soft and comfortable. You decide to lay down, and then close your eyes, falling into a comfortable sleep.")
    input()
    print("The end.")
    input()
    print("Your game ID is %s. If you play the Floor 2 or Floor 3 games for YAAG Murder Mystery Edition, you’ll need it." % epilogueID)
    input()
    print("Thank you very much for playing my game! It was lots of fun to write, develop and all that. I’ll be making more in the future, so keep an eye out for that!")
    input()
  elif epilogueID == 2:
    print("You regain your vision and see a crowd of people cheering for you and the rest of your team.")
    input()
    print("Then, the crowd starts gasping and murmuring.")
    input()
    print("“They noticed MurdleMuffin’s not here.” said Eevee005.")
    input()
    print("You hear a voice. The same voice from the beginning of the run.")
    input()
    print("“Welcome, welcome! My name is PSugar400, and congratulations! You’ve defeated the first floor of the dungeon, and we’re all excited for you! Wait, there’s only four of you?” said PSugar400.")
    input()
    print("“Uh, yeah. MurdleMuffin was killed by enemies.” you say.")
    input()
    print("“Oh, I see. Let us take a moment to remember them.” said PSugar400. “I have a memorial letter written for them. I’ll read it.”")
    input()
    print("“We don’t know much about MurdleMuffin’s intentions. They were quite shy, and wouldn’t reveal much. We believe it was to meet new people. Let us take a moment of silence.” The crowd was silent for a minute.")
    input()
    print("“Now, as the audience knows, we have a special guest! The Chosen Hero, %s, is here today with us!” the audience roars with cheers, and %s stands up, waving, and thanking the crowd." % heroName)
    input()
    print("%s looks at you and your team. You notice they look at you suspiciously. But soon looks away." % heroName)
    input()
    print("PSugar400 continues. “But back to our team of playtesters, I think they deserve a name, what do you reckon?” The stands agrees loudly. “How about the Fighting Team?” the audience cheers out the name.")
    input()
    print("“Fighting Team! Fighting Team! Fighting Team! Fighting Team! Fighting Team! Fighting Team!”")
    input()
    print("The Arm Attack Inc. Staff calm down the audience before PSugar400 then keeps talking. ”Now, let’s hear a word from each member from our newly named Fighting Team. %s, you’re up first." & playerName)
    input()
    message = input("Type a message to tell the audience. ")
    print("“%s” you say." % message)
    input()
    print("“Thank you for your words, %s.”" % message)
    input()
    print("Tear 2bad, what would you like to say to the audience?” asked PSugar400.")
    input()
    print("“It. Was. AWESOME! I was slashing enemies, taking them down, I carried the team. Okay, well, maybe not. But still, it was great.” he answered. “Wow, what a story!” ")
    input()
    print("“Dino-Pack, what do you have to say?” said PSugar400, continuing her interview.")
    input()
    print("“I ended up finding some really good gear. Gave some extras to other team members. Fighting enemies was really fun! I’d love to do the other floors.” he told PSugar400.")
    input()
    print("“Well, in the future, you might actually be able to do the other 2 floors!” she told Dino-Pack.")
    input()
    print("“And now, last but not least is Eevee005! What are your thoughts on the entire thing?”")
    input()
    print("“Much like the rest of us, I enjoyed it very much. Although If you have the ability to, the Arm Attack Inc. team should make the boss more powerful. I took down the boss by myself, and I think that the entire team, if not, at least more than one person should be required to take down Mr. Dye.” she said.")
    input()
    print("“Alright, I’ll pass that feedback on to the Development team. Thank you!” said PSugar400.")
    input()
    print("“And that was the Multiplayer Test for our dungeon here at Arm Attack Inc.! Thank you for being here. Once again, I’ve been your host, PSugar400, and goodnight.” finished PSugar400, walking up to you and your team.")
    input()
    print("“Tomorrow you all have meetings with %s. We’ve given you rooms in a nearby hotel. You all will have reps that you’ll work with. For now, follow the security team. They’ll lead you to your hotel rooms.” informed PSugar400." % heroName)
    input()
    print("You follow PSugar400’s lead along with the rest of your team. Eventually, PSugar400 breaks off the path, and you instead follow a security team. They bring you past crowds, so you wave to them.")
    input()
    print("After a good 10 minutes, you arrive at your hotel room. Room 105. It’s not a very large room, but bigger than general hotel rooms. You look at the time, it’s only 3:00 PM, but you feel very tired. The bed looks very soft and comfortable. You decide to lay down, and then close your eyes, falling into a comfortable sleep.")
    input()
    print("The end.")
    input()
    print("Your game ID is %s. If you play the Floor 2 or Floor 3 games for YAAG Murder Mystery Edition, you’ll need it." % epilogueID)
    input()
    print("Thank you very much for playing my game! It was lots of fun to write, develop and all that. I’ll be making more in the future, so keep an eye out for that!")
    input()
  elif epilogueID == 3:
    print("You regain your vision and see a crowd of people cheering for you and the rest of your team.")
    input()
    print("Then, the crowd starts gasping and murmuring.")
    input()
    print("“They noticed Dino-Pack’s not here.” said Eevee005.")
    input()
    print("You hear a voice. The same voice from the beginning of the run.")
    input()
    print("“Welcome, welcome! My name is PSugar400, and congratulations! You’ve defeated the first floor of the dungeon, and we’re all excited for you! Wait, there’s only four of you?” said PSugar400.")
    input()
    print("“Uh, yeah. Dino-Pack was killed by enemies.” you say.")
    input()
    print("“Oh, I see. Let us take a moment to remember them.” said PSugar400. “I have a memorial letter written for them. I’ll read it.”")
    input()
    print("“Dino-Pack was always interested in Arm Attack Inc.’s projects. He was interested in the original Dungeon run. Trying to talk to %s, trying to talk to us, and all that. The second we saw his request to join the test, we knew he had to be accepted. Let us take a moment of silence.” The crowd was silent for a minute." % heroName)
    input()
    print("“Now, as the audience knows, we have a special guest! The Chosen Hero, %s, is here today with us!” the audience roars with cheers, and %s stands up, waving, and thanking the crowd." % heroName)
    input()
    print("%s looks at you and your team. You notice they look at you suspiciously. But soon looks away." % heroName)
    input()
    print("PSugar400 continues. “But back to our team of playtesters, I think they deserve a name, what do you reckon?” The stands agrees loudly. “How about the Fighting Team?” the audience cheers out the name.")
    input()
    print("“Fighting Team! Fighting Team! Fighting Team! Fighting Team! Fighting Team! Fighting Team!”")
    input()
    print("The Arm Attack Inc. Staff calm down the audience before PSugar400 then keeps talking. ”Now, let’s hear a word from each member from our newly named Fighting Team. %s, you’re up first." & playerName)
    input()
    message = input("Type a message to tell the audience. ")
    print("“%s” you say." % message)
    input()
    print("“Thank you for your words, %s.”" % message)
    input()
    print("Tear 2bad, what would you like to say to the audience?” asked PSugar400.")
    input()
    print("“It. Was. AWESOME! I was slashing enemies, taking them down, I carried the team. Okay, well, maybe not. But still, it was great.” he answered. “Wow, what a story!” ")
    input()
    print("“So, MurdleMuffin, what do you think?” asked PSugar400.")
    input()
    print("“It was fine, I kinda just stayed in one room for a while.” he answered simply. “Oh. Well, that’s okay.”")
    input()
    print("“And now, last but not least is Eevee005! What are your thoughts on the entire thing?”")
    input()
    print("“Much like the rest of us, I enjoyed it very much. Although If you have the ability to, the Arm Attack Inc. team should make the boss more powerful. I took down the boss by myself, and I think that the entire team, if not, at least more than one person should be required to take down Mr. Dye.” she said.")
    input()
    print("“Alright, I’ll pass that feedback on to the Development team. Thank you!” said PSugar400.")
    input()
    print("“And that was the Multiplayer Test for our dungeon here at Arm Attack Inc.! Thank you for being here. Once again, I’ve been your host, PSugar400, and goodnight.” finished PSugar400, walking up to you and your team.")
    input()
    print("“Tomorrow you all have meetings with %s. We’ve given you rooms in a nearby hotel. You all will have reps that you’ll work with. For now, follow the security team. They’ll lead you to your hotel rooms.” informed PSugar400." % heroName)
    input()
    print("You follow PSugar400’s lead along with the rest of your team. Eventually, PSugar400 breaks off the path, and you instead follow a security team. They bring you past crowds, so you wave to them.")
    input()
    print("After a good 10 minutes, you arrive at your hotel room. Room 105. It’s not a very large room, but bigger than general hotel rooms. You look at the time, it’s only 3:00 PM, but you feel very tired. The bed looks very soft and comfortable. You decide to lay down, and then close your eyes, falling into a comfortable sleep.")
    input()
    print("The end.")
    input()
    print("Your game ID is %s. If you play the Floor 2 or Floor 3 games for YAAG Murder Mystery Edition, you’ll need it." % epilogueID)
    input()
    print("Thank you very much for playing my game! It was lots of fun to write, develop and all that. I’ll be making more in the future, so keep an eye out for that!")
    input()
  elif epilogueID == 4:
    print("You regain your vision and see a crowd of people cheering for you and the rest of your team.")
    input()
    print("Then, the crowd starts gasping and murmuring.")
    input()
    print("“They noticed Eevee005’s not here.” said Dino-Pack.")
    input()
    print("You hear a voice. The same voice from the beginning of the run.")
    input()
    print("“Welcome, welcome! My name is PSugar400, and congratulations! You’ve defeated the first floor of the dungeon, and we’re all excited for you! Wait, there’s only four of you?” said PSugar400.")
    input()
    print("“Uh, yeah. Eevee005 was killed by enemies.” you say.")
    input()
    print("“Oh, I see. Let us take a moment to remember them.” said PSugar400. “I have a memorial letter written for them. I’ll read it.”")
    input()
    print("“Eevee005 was a sweet person. She was legitimately interested in the project. She was keeping up with updates as soon as we posted an announcement that we were looking for people for a multiplayer test. Let us take a moment of silence.” The crowd was silent for a minute.")
    input()
    print("“Now, as the audience knows, we have a special guest! The Chosen Hero, %s, is here today with us!” the audience roars with cheers, and %s stands up, waving, and thanking the crowd." % heroName)
    input()
    print("%s looks at you and your team. You notice they look at you suspiciously. But soon looks away." % heroName)
    input()
    print("PSugar400 continues. “But back to our team of playtesters, I think they deserve a name, what do you reckon?” The stands agrees loudly. “How about the Fighting Team?” the audience cheers out the name.")
    input()
    print("“Fighting Team! Fighting Team! Fighting Team! Fighting Team! Fighting Team! Fighting Team!”")
    input()
    print("The Arm Attack Inc. Staff calm down the audience before PSugar400 then keeps talking. ”Now, let’s hear a word from each member from our newly named Fighting Team. %s, you’re up first." & playerName)
    input()
    message = input("Type a message to tell the audience. ")
    print("“%s” you say." % message)
    input()
    print("“Thank you for your words, %s.”" % message)
    input()
    print("Tear 2bad, what would you like to say to the audience?” asked PSugar400.")
    input()
    print("“It. Was. AWESOME! I was slashing enemies, taking them down, I carried the team. Okay, well, maybe not. But still, it was great.” he answered. “Wow, what a story!” ")
    input()
    print("“So, MurdleMuffin, what do you think?” asked PSugar400.")
    input()
    print("“It was fine, I kinda just stayed in one room for a while.” he answered simply. “Oh. Well, that’s okay.”")
    input()
    print("“Dino-Pack, what do you have to say?” said PSugar400, continuing her interview.")
    input()
    print("“I ended up finding some really good gear. Gave some extras to other team members. Fighting enemies was really fun! I’d love to do the other floors.” he told PSugar400.")
    input()
    print("“Well, in the future, you might actually be able to do the other 2 floors!” she told Dino-Pack.")
    input()
    print("“And that was the Multiplayer Test for our dungeon here at Arm Attack Inc.! Thank you for being here. Once again, I’ve been your host, PSugar400, and goodnight.” finished PSugar400, walking up to you and your team.")
    input()
    print("“Tomorrow you all have meetings with %s. We’ve given you rooms in a nearby hotel. You all will have reps that you’ll work with. For now, follow the security team. They’ll lead you to your hotel rooms.” informed PSugar400." % heroName)
    input()
    print("You follow PSugar400’s lead along with the rest of your team. Eventually, PSugar400 breaks off the path, and you instead follow a security team. They bring you past crowds, so you wave to them.")
    input()
    print("After a good 10 minutes, you arrive at your hotel room. Room 105. It’s not a very large room, but bigger than general hotel rooms. You look at the time, it’s only 3:00 PM, but you feel very tired. The bed looks very soft and comfortable. You decide to lay down, and then close your eyes, falling into a comfortable sleep.")
    input()
    print("The end.")
    input()
    print("Your game ID is %s. If you play the Floor 2 or Floor 3 games for YAAG Murder Mystery Edition, you’ll need it." % epilogueID)
    input()
    print("Thank you very much for playing my game! It was lots of fun to write, develop and all that. I’ll be making more in the future, so keep an eye out for that!")
    input()
  elif epilogueID == 5:
    print("You regain your vision and see a crowd of people cheering for you and the rest of your team.")
    input()
    print("Then, the crowd starts gasping and murmuring.")
    input()
    print("“They noticed Tear 2bad’s not here.” said Eevee005.")
    input()
    print("“And they noticed MurdleMuffin’s not here.” said Eevee005.")
    input()
    print("You hear a voice. The same voice from the beginning of the run.")
    input()
    print("“Welcome, welcome! My name is PSugar400, and congratulations! You’ve defeated the first floor of the dungeon, and we’re all excited for you! Wait, there’s only three of you?” said PSugar400.")
    input()
    print("“Uh, yeah. Tear 2bad was killed by enemies.” you say.")
    input()
    print("“So was MurdleMuffin.” you say.")
    input()
    print("“Oh, I see. Let us take a moment to remember them.” said PSugar400. “I have a memorial letter written for them. I’ll read it.”")
    input()
    print("“Tear 2bad was one of the first to sign up for our multiplayer test. He was very excited to play in this game. He said he liked the fighting aspect of the test. He wanted to fight Mr. Dye as soon as possible. Let us take a moment of silence.” The crowd was silent for a minute.")
    input()
    print("“We don’t know much about MurdleMuffin’s intentions. They were quite shy, and wouldn’t reveal much. We believe it was to meet new people. Let us take a moment of silence.” The crowd was silent for a minute.")
    input()
    print("“Now, as the audience knows, we have a special guest! The Chosen Hero, %s, is here today with us!” the audience roars with cheers, and %s stands up, waving, and thanking the crowd." % heroName)
    input()
    print("%s looks at you and your team. You notice they look at you suspiciously. But soon looks away." % heroName)
    input()
    print("PSugar400 continues. “But back to our team of playtesters, I think they deserve a name, what do you reckon?” The stands agrees loudly. “How about the Fighting Team?” the audience cheers out the name.")
    input()
    print("“Fighting Team! Fighting Team! Fighting Team! Fighting Team! Fighting Team! Fighting Team!”")
    input()
    print("The Arm Attack Inc. Staff calm down the audience before PSugar400 then keeps talking. ”Now, let’s hear a word from each member from our newly named Fighting Team. %s, you’re up first." % playerName)
    input()
    message = input("Type a message to tell the audience. ")
    print("“%s” you say." % message)
    input()
    print("“Thank you for your words, %s.”" % message)
    input()
    print("“Dino-Pack, what do you have to say?” said PSugar400, continuing her interview.")
    input()
    print("“I ended up finding some really good gear. Gave some extras to other team members. Fighting enemies was really fun! I’d love to do the other floors.” he told PSugar400.")
    input()
    print("“Well, in the future, you might actually be able to do the other 2 floors!” she told Dino-Pack.")
    input()
    print("“And now, last but not least is Eevee005! What are your thoughts on the entire thing?”")
    input()
    print("“Much like the rest of us, I enjoyed it very much. Although If you have the ability to, the Arm Attack Inc. team should make the boss more powerful. I took down the boss by myself, and I think that the entire team, if not, at least more than one person should be required to take down Mr. Dye.” she said.")
    input()
    print("“Alright, I’ll pass that feedback on to the Development team. Thank you!” said PSugar400.")
    input()
    print("“And that was the Multiplayer Test for our dungeon here at Arm Attack Inc.! Thank you for being here. Once again, I’ve been your host, PSugar400, and goodnight.” finished PSugar400, walking up to you and your team.")
    input()
    print("“Tomorrow you all have meetings with %s. We’ve given you rooms in a nearby hotel. You all will have reps that you’ll work with. For now, follow the security team. They’ll lead you to your hotel rooms.” informed PSugar400." % heroName)
    input()
    print("You follow PSugar400’s lead along with the rest of your team. Eventually, PSugar400 breaks off the path, and you instead follow a security team. They bring you past crowds, so you wave to them.")
    input()
    print("After a good 10 minutes, you arrive at your hotel room. Room 105. It’s not a very large room, but bigger than general hotel rooms. You look at the time, it’s only 3:00 PM, but you feel very tired. The bed looks very soft and comfortable. You decide to lay down, and then close your eyes, falling into a comfortable sleep.")
    input()
    print("The end.")
    input()
    print("Your game ID is %s. If you play the Floor 2 or Floor 3 games for YAAG Murder Mystery Edition, you’ll need it." % epilogueID)
    input()
    print("Thank you very much for playing my game! It was lots of fun to write, develop and all that. I’ll be making more in the future, so keep an eye out for that!")
    input()
  elif epilogueID == 6:
    print("You regain your vision and see a crowd of people cheering for you and the rest of your team.")
    input()
    print("Then, the crowd starts gasping and murmuring.")
    input()
    print("“They noticed Tear 2bad’s not here.” said Eevee005.")
    input()
    print("“They noticed Dino-Pack’s not here.” said Eevee005.")
    input()
    print("You hear a voice. The same voice from the beginning of the run.")
    input()
    print("“Welcome, welcome! My name is PSugar400, and congratulations! You’ve defeated the first floor of the dungeon, and we’re all excited for you! Wait, there’s only three of you?” said PSugar400.")
    input()
    print("“Uh, yeah. Tear 2bad was killed by enemies.” you say.")
    input()
    print("“Uh, yeah. Dino-Pack was killed by enemies.” you say.")
    input()
    print("“Oh, I see. Let us take a moment to remember them.” said PSugar400. “I have a memorial letter written for them. I’ll read it.”")
    input()
    print("“Tear 2bad was one of the first to sign up for our multiplayer test. He was very excited to play in this game. He said he liked the fighting aspect of the test. He wanted to fight Mr. Dye as soon as possible. Let us take a moment of silence.” The crowd was silent for a minute.")
    input()
    print("“Dino-Pack was always interested in Arm Attack Inc.’s projects. He was interested in the original Dungeon run. Trying to talk to %s, trying to talk to us, and all that. The second we saw his request to join the test, we knew he had to be accepted. Let us take a moment of silence.” The crowd was silent for a minute." % heroName)
    input()
    print("“Now, as the audience knows, we have a special guest! The Chosen Hero, %s, is here today with us!” the audience roars with cheers, and %s stands up, waving, and thanking the crowd." % heroName)
    input()
    print("%s looks at you and your team. You notice they look at you suspiciously. But soon looks away." % heroName)
    input()
    print("PSugar400 continues. “But back to our team of playtesters, I think they deserve a name, what do you reckon?” The stands agrees loudly. “How about the Fighting Team?” the audience cheers out the name.")
    input()
    print("“Fighting Team! Fighting Team! Fighting Team! Fighting Team! Fighting Team! Fighting Team!”")
    input()
    print("The Arm Attack Inc. Staff calm down the audience before PSugar400 then keeps talking. ”Now, let’s hear a word from each member from our newly named Fighting Team. %s, you’re up first." % playerName)
    input()
    message = input("Type a message to tell the audience. ")
    print("“%s” you say." % message)
    input()
    print("“Thank you for your words, %s.”" % message)
    input()
    print("“So, MurdleMuffin, what do you think?” asked PSugar400.")
    input()
    print("“It was fine, I kinda just stayed in one room for a while.” he answered simply. “Oh. Well, that’s okay.”")
    input()
    print("“And now, last but not least is Eevee005! What are your thoughts on the entire thing?”")
    input()
    print("“Much like the rest of us, I enjoyed it very much. Although If you have the ability to, the Arm Attack Inc. team should make the boss more powerful. I took down the boss by myself, and I think that the entire team, if not, at least more than one person should be required to take down Mr. Dye.” she said.")
    input()
    print("“Alright, I’ll pass that feedback on to the Development team. Thank you!” said PSugar400.")
    input()
    input()
    print("“And that was the Multiplayer Test for our dungeon here at Arm Attack Inc.! Thank you for being here. Once again, I’ve been your host, PSugar400, and goodnight.” finished PSugar400, walking up to you and your team.")
    input()
    print("“Tomorrow you all have meetings with %s. We’ve given you rooms in a nearby hotel. You all will have reps that you’ll work with. For now, follow the security team. They’ll lead you to your hotel rooms.” informed PSugar400." % heroName)
    input()
    print("You follow PSugar400’s lead along with the rest of your team. Eventually, PSugar400 breaks off the path, and you instead follow a security team. They bring you past crowds, so you wave to them.")
    input()
    print("After a good 10 minutes, you arrive at your hotel room. Room 105. It’s not a very large room, but bigger than general hotel rooms. You look at the time, it’s only 3:00 PM, but you feel very tired. The bed looks very soft and comfortable. You decide to lay down, and then close your eyes, falling into a comfortable sleep.")
    input()
    print("The end.")
    input()
    print("Your game ID is %s. If you play the Floor 2 or Floor 3 games for YAAG Murder Mystery Edition, you’ll need it." % epilogueID)
    input()
    print("Thank you very much for playing my game! It was lots of fun to write, develop and all that. I’ll be making more in the future, so keep an eye out for that!")
    input()
  elif epilogueID == 7:
    print("You regain your vision and see a crowd of people cheering for you and the rest of your team.")
    input()
    print("Then, the crowd starts gasping and murmuring.")
    input()
    print("“They noticed Tear 2bad’s not here.” said Eevee005.")
    input()
    print("“They noticed Eevee005’s not here.” said Dino-Pack.")
    input()
    print("You hear a voice. The same voice from the beginning of the run.")
    input()
    print("“Welcome, welcome! My name is PSugar400, and congratulations! You’ve defeated the first floor of the dungeon, and we’re all excited for you! Wait, there’s only three of you?” said PSugar400.")
    input()
    print("“Uh, yeah. Tear 2bad was killed by enemies.” you say.")
    input()
    print("“Uh, yeah. Eevee005 was killed by enemies.” you say.")
    input()
    print("“Oh, I see. Let us take a moment to remember them.” said PSugar400. “I have a memorial letter written for them. I’ll read it.”")
    input()
    print("“Tear 2bad was one of the first to sign up for our multiplayer test. He was very excited to play in this game. He said he liked the fighting aspect of the test. He wanted to fight Mr. Dye as soon as possible. Let us take a moment of silence.” The crowd was silent for a minute.")
    input()
    print("“Eevee005 was a sweet person. She was legitimately interested in the project. She was keeping up with updates as soon as we posted an announcement that we were looking for people for a multiplayer test. Let us take a moment of silence.” The crowd was silent for a minute.")
    input()
    print("“Now, as the audience knows, we have a special guest! The Chosen Hero, %s, is here today with us!” the audience roars with cheers, and %s stands up, waving, and thanking the crowd." % heroName)
    input()
    print("%s looks at you and your team. You notice they look at you suspiciously. But soon looks away." % heroName)
    input()
    print("PSugar400 continues. “But back to our team of playtesters, I think they deserve a name, what do you reckon?” The stands agrees loudly. “How about the Fighting Team?” the audience cheers out the name.")
    input()
    print("“Fighting Team! Fighting Team! Fighting Team! Fighting Team! Fighting Team! Fighting Team!”")
    input()
    print("The Arm Attack Inc. Staff calm down the audience before PSugar400 then keeps talking. ”Now, let’s hear a word from each member from our newly named Fighting Team. %s, you’re up first." % playerName)
    input()
    message = input("Type a message to tell the audience. ")
    print("“%s” you say." % message)
    input()
    print("“Thank you for your words, %s.”" % message)
    input()
    print("“So, MurdleMuffin, what do you think?” asked PSugar400.")
    input()
    print("“It was fine, I kinda just stayed in one room for a while.” he answered simply. “Oh. Well, that’s okay.”")
    input()
    print("“Dino-Pack, what do you have to say?” said PSugar400, continuing her interview.")
    input()
    print("“I ended up finding some really good gear. Gave some extras to other team members. Fighting enemies was really fun! I’d love to do the other floors.” he told PSugar400.")
    input()
    print("“Well, in the future, you might actually be able to do the other 2 floors!” she told Dino-Pack.")
    input()
    print("“And that was the Multiplayer Test for our dungeon here at Arm Attack Inc.! Thank you for being here. Once again, I’ve been your host, PSugar400, and goodnight.” finished PSugar400, walking up to you and your team.")
    input()
    print("“Tomorrow you all have meetings with %s. We’ve given you rooms in a nearby hotel. You all will have reps that you’ll work with. For now, follow the security team. They’ll lead you to your hotel rooms.” informed PSugar400." % heroName)
    input()
    print("You follow PSugar400’s lead along with the rest of your team. Eventually, PSugar400 breaks off the path, and you instead follow a security team. They bring you past crowds, so you wave to them.")
    input()
    print("After a good 10 minutes, you arrive at your hotel room. Room 105. It’s not a very large room, but bigger than general hotel rooms. You look at the time, it’s only 3:00 PM, but you feel very tired. The bed looks very soft and comfortable. You decide to lay down, and then close your eyes, falling into a comfortable sleep.")
    input()
    print("The end.")
    input()
    print("Your game ID is %s. If you play the Floor 2 or Floor 3 games for YAAG Murder Mystery Edition, you’ll need it." % epilogueID)
    input()
    print("Thank you very much for playing my game! It was lots of fun to write, develop and all that. I’ll be making more in the future, so keep an eye out for that!")
    input()
  elif epilogueID == 8:
    print("You regain your vision and see a crowd of people cheering for you and the rest of your team.")
    input()
    print("Then, the crowd starts gasping and murmuring.")
    input()
    print("“They noticed MurdleMuffin’s not here.” said Eevee005.")
    input()
    print("“They noticed Dino-Pack’s not here.” said Eevee005.")
    input()
    print("You hear a voice. The same voice from the beginning of the run.")
    input()
    print("“Welcome, welcome! My name is PSugar400, and congratulations! You’ve defeated the first floor of the dungeon, and we’re all excited for you! Wait, there’s only three of you?” said PSugar400.")
    input()
    print("“Uh, yeah. MurdleMuffin was killed by enemies.” you say.")
    input()
    print("“Uh, yeah. Dino-Pack was killed by enemies.” you say.")
    input()
    print("“Oh, I see. Let us take a moment to remember them.” said PSugar400. “I have a memorial letter written for them. I’ll read it.”")
    input()
    print("“We don’t know much about MurdleMuffin’s intentions. They were quite shy, and wouldn’t reveal much. We believe it was to meet new people. Let us take a moment of silence.” The crowd was silent for a minute.")
    input()
    print("“Dino-Pack was always interested in Arm Attack Inc.’s projects. He was interested in the original Dungeon run. Trying to talk to %s, trying to talk to us, and all that. The second we saw his request to join the test, we knew he had to be accepted. Let us take a moment of silence.” The crowd was silent for a minute." % heroName)
    input()
    print("“Now, as the audience knows, we have a special guest! The Chosen Hero, %s, is here today with us!” the audience roars with cheers, and %s stands up, waving, and thanking the crowd." % heroName)
    input()
    print("%s looks at you and your team. You notice they look at you suspiciously. But soon looks away." % heroName)
    input()
    print("PSugar400 continues. “But back to our team of playtesters, I think they deserve a name, what do you reckon?” The stands agrees loudly. “How about the Fighting Team?” the audience cheers out the name.")
    input()
    print("“Fighting Team! Fighting Team! Fighting Team! Fighting Team! Fighting Team! Fighting Team!”")
    input()
    print("The Arm Attack Inc. Staff calm down the audience before PSugar400 then keeps talking. ”Now, let’s hear a word from each member from our newly named Fighting Team. %s, you’re up first." & playerName)
    input()
    message = input("Type a message to tell the audience. ")
    print("“%s” you say." % message)
    input()
    print("“Thank you for your words, %s.”" % message)
    input()
    print("Tear 2bad, what would you like to say to the audience?” asked PSugar400.")
    input()
    print("“It. Was. AWESOME! I was slashing enemies, taking them down, I carried the team. Okay, well, maybe not. But still, it was great.” he answered. “Wow, what a story!” ")
    input()
    print("“And now, last but not least is Eevee005! What are your thoughts on the entire thing?”")
    input()
    print("“Much like the rest of us, I enjoyed it very much. Although If you have the ability to, the Arm Attack Inc. team should make the boss more powerful. I took down the boss by myself, and I think that the entire team, if not, at least more than one person should be required to take down Mr. Dye.” she said.")
    input()
    print("“Alright, I’ll pass that feedback on to the Development team. Thank you!” said PSugar400.")
    input()
    print("“And that was the Multiplayer Test for our dungeon here at Arm Attack Inc.! Thank you for being here. Once again, I’ve been your host, PSugar400, and goodnight.” finished PSugar400, walking up to you and your team.")
    input()
    print("“Tomorrow you all have meetings with %s. We’ve given you rooms in a nearby hotel. You all will have reps that you’ll work with. For now, follow the security team. They’ll lead you to your hotel rooms.” informed PSugar400." % heroName)
    input()
    print("You follow PSugar400’s lead along with the rest of your team. Eventually, PSugar400 breaks off the path, and you instead follow a security team. They bring you past crowds, so you wave to them.")
    input()
    print("After a good 10 minutes, you arrive at your hotel room. Room 105. It’s not a very large room, but bigger than general hotel rooms. You look at the time, it’s only 3:00 PM, but you feel very tired. The bed looks very soft and comfortable. You decide to lay down, and then close your eyes, falling into a comfortable sleep.")
    input()
    print("The end.")
    input()
    print("Your game ID is %s. If you play the Floor 2 or Floor 3 games for YAAG Murder Mystery Edition, you’ll need it." % epilogueID)
    input()
    print("Thank you very much for playing my game! It was lots of fun to write, develop and all that. I’ll be making more in the future, so keep an eye out for that!")
    input()
  elif epilogueID == 9:
    print("You regain your vision and see a crowd of people cheering for you and the rest of your team.")
    input()
    print("Then, the crowd starts gasping and murmuring.")
    input()
    print("“They noticed Dino-Pack’s not here.” said MurdleMuffin.")
    input()
    print("“They noticed Eevee005’s not here.” said Tear 2bad.")
    input()
    print("You hear a voice. The same voice from the beginning of the run.")
    input()
    print("“Welcome, welcome! My name is PSugar400, and congratulations! You’ve defeated the first floor of the dungeon, and we’re all excited for you! Wait, there’s only three of you?” said PSugar400.")
    input()
    print("“Uh, yeah. Dino-Pack was killed by enemies.” you say.")
    input()
    print("“Uh, yeah. Eevee005 was killed by enemies.” you say.")
    input()
    print("“Oh, I see. Let us take a moment to remember them.” said PSugar400. “I have a memorial letter written for them. I’ll read it.”")
    input()
    print("“Dino-Pack was always interested in Arm Attack Inc.’s projects. He was interested in the original Dungeon run. Trying to talk to %s, trying to talk to us, and all that. The second we saw his request to join the test, we knew he had to be accepted. Let us take a moment of silence.” The crowd was silent for a minute." % heroName)
    input()
    print("“Eevee005 was a sweet person. She was legitimately interested in the project. She was keeping up with updates as soon as we posted an announcement that we were looking for people for a multiplayer test. Let us take a moment of silence.” The crowd was silent for a minute.")
    input()
    input()
    print("“Now, as the audience knows, we have a special guest! The Chosen Hero, %s, is here today with us!” the audience roars with cheers, and %s stands up, waving, and thanking the crowd." % heroName)
    input()
    print("%s looks at you and your team. You notice they look at you suspiciously. But soon looks away." % heroName)
    input()
    print("PSugar400 continues. “But back to our team of playtesters, I think they deserve a name, what do you reckon?” The stands agrees loudly. “How about the Fighting Team?” the audience cheers out the name.")
    input()
    print("“Fighting Team! Fighting Team! Fighting Team! Fighting Team! Fighting Team! Fighting Team!”")
    input()
    print("The Arm Attack Inc. Staff calm down the audience before PSugar400 then keeps talking. ”Now, let’s hear a word from each member from our newly named Fighting Team. %s, you’re up first." % playerName)
    input()
    message = input("Type a message to tell the audience. ")
    print("“%s” you say." % message)
    input()
    print("“Thank you for your words, %s.”" % message)
    input()
    print("Tear 2bad, what would you like to say to the audience?” asked PSugar400.")
    input()
    print("“It. Was. AWESOME! I was slashing enemies, taking them down, I carried the team. Okay, well, maybe not. But still, it was great.” he answered. “Wow, what a story!”")
    input()
    print("“So, MurdleMuffin, what do you think?” asked PSugar400.")
    input()
    print("“It was fine, I kinda just stayed in one room for a while.” he answered simply. “Oh. Well, that’s okay.”")
    input()
    print("“And that was the Multiplayer Test for our dungeon here at Arm Attack Inc.! Thank you for being here. Once again, I’ve been your host, PSugar400, and goodnight.” finished PSugar400, walking up to you and your team.")
    input()
    print("“Tomorrow you all have meetings with %s. We’ve given you rooms in a nearby hotel. You all will have reps that you’ll work with. For now, follow the security team. They’ll lead you to your hotel rooms.” informed PSugar400." % heroName)
    input()
    print("You follow PSugar400’s lead along with the rest of your team. Eventually, PSugar400 breaks off the path, and you instead follow a security team. They bring you past crowds, so you wave to them.")
    input()
    print("After a good 10 minutes, you arrive at your hotel room. Room 105. It’s not a very large room, but bigger than general hotel rooms. You look at the time, it’s only 3:00 PM, but you feel very tired. The bed looks very soft and comfortable. You decide to lay down, and then close your eyes, falling into a comfortable sleep.")
    input()
    print("The end.")
    input()
    print("Your game ID is %s. If you play the Floor 2 or Floor 3 games for YAAG Murder Mystery Edition, you’ll need it." % epilogueID)
    input()
    print("Thank you very much for playing my game! It was lots of fun to write, develop and all that. I’ll be making more in the future, so keep an eye out for that!")
    input()
  elif epilogueID == 10:
    print("You regain your vision and see a crowd of people cheering for you and the rest of your team.")
    input()
    print("Then, the crowd starts gasping and murmuring.")
    input()
    print("“They noticed Tear 2bad’s not here.” said Eevee005.")
    input()
    print("“They noticed MurdleMuffin’s not here.” said Eevee005.")
    input()
    print("“They noticed Dino-Pack’s not here.” said Eevee005.")
    input()
    print("You hear a voice. The same voice from the beginning of the run.")
    input()
    print("“Welcome, welcome! My name is PSugar400, and congratulations! You’ve defeated the first floor of the dungeon, and we’re all excited for you! Wait, there’s only three of you?” said PSugar400.")
    input()
    print("“Uh, yeah. Tear 2bad was killed by enemies.” you say.")
    input()
    print("“Uh, yeah. MurdleMuffin was killed by enemies.” you say.")
    input()
    print("“Uh, yeah. Dino-Pack was killed by enemies.” you say.")
    input()
    print("“Oh, I see. Let us take a moment to remember them.” said PSugar400. “I have a memorial letter written for them. I’ll read it.”")
    input()
    print("“Tear 2bad was one of the first to sign up for our multiplayer test. He was very excited to play in this game. He said he liked the fighting aspect of the test. He wanted to fight Mr. Dye as soon as possible. Let us take a moment of silence.” The crowd was silent for a minute.")
    input()
    print("“We don’t know much about MurdleMuffin’s intentions. They were quite shy, and wouldn’t reveal much. We believe it was to meet new people. Let us take a moment of silence.” The crowd was silent for a minute.")
    input()
    print("“Dino-Pack was always interested in Arm Attack Inc.’s projects. He was interested in the original Dungeon run. Trying to talk to %s, trying to talk to us, and all that. The second we saw his request to join the test, we knew he had to be accepted. Let us take a moment of silence.” The crowd was silent for a minute." % heroName)
    input()
    print("“Now, as the audience knows, we have a special guest! The Chosen Hero, %s, is here today with us!” the audience roars with cheers, and %s stands up, waving, and thanking the crowd." % heroName)
    input()
    print("%s looks at you and your team. You notice they look at you suspiciously. But soon looks away." % heroName)
    input()
    print("PSugar400 continues. “But back to our team of playtesters, I think they deserve a name, what do you reckon?” The stands agrees loudly. “How about the Fighting Team?” the audience cheers out the name.")
    input()
    print("“Fighting Team! Fighting Team! Fighting Team! Fighting Team! Fighting Team! Fighting Team!”")
    input()
    print("The Arm Attack Inc. Staff calm down the audience before PSugar400 then keeps talking. ”Now, let’s hear a word from each member from our newly named Fighting Team. %s, you’re up first." & playerName)
    input()
    message = input("Type a message to tell the audience. ")
    print("“%s” you say." % message)
    input()
    print("“Thank you for your words, %s.”" % message)
    input()
    print("“And now, last but not least is Eevee005! What are your thoughts on the entire thing?”")
    input()
    print("“Much like the rest of us, I enjoyed it very much. Although If you have the ability to, the Arm Attack Inc. team should make the boss more powerful. I took down the boss by myself, and I think that the entire team, if not, at least more than one person should be required to take down Mr. Dye.” she said.")
    input()
    print("“Alright, I’ll pass that feedback on to the Development team. Thank you!” said PSugar400.")
    input()
    input()
    print("“And that was the Multiplayer Test for our dungeon here at Arm Attack Inc.! Thank you for being here. Once again, I’ve been your host, PSugar400, and goodnight.” finished PSugar400, walking up to you and your team.")
    input()
    print("“Tomorrow you all have meetings with %s. We’ve given you rooms in a nearby hotel. You all will have reps that you’ll work with. For now, follow the security team. They’ll lead you to your hotel rooms.” informed PSugar400." % heroName)
    input()
    print("You follow PSugar400’s lead along with the rest of your team. Eventually, PSugar400 breaks off the path, and you instead follow a security team. They bring you past crowds, so you wave to them.")
    input()
    print("After a good 10 minutes, you arrive at your hotel room. Room 105. It’s not a very large room, but bigger than general hotel rooms. You look at the time, it’s only 3:00 PM, but you feel very tired. The bed looks very soft and comfortable. You decide to lay down, and then close your eyes, falling into a comfortable sleep.")
    input()
    print("The end.")
    input()
    print("Your game ID is %s. If you play the Floor 2 or Floor 3 games for YAAG Murder Mystery Edition, you’ll need it." % epilogueID)
    input()
    print("Thank you very much for playing my game! It was lots of fun to write, develop and all that. I’ll be making more in the future, so keep an eye out for that!")
    input()
  elif epilogueID == 11:
    print("You regain your vision and see a crowd of people cheering for you and the rest of your team.")
    input()
    print("Then, the crowd starts gasping and murmuring.")
    input()
    print("“They noticed Tear 2bad’s not here.” said Dino-Pack.")
    input()
    print("“They noticed MurdleMuffin’s not here.” said Dino-Pack.")
    input()
    print("“They noticed Eevee005’s not here.” said Dino-Pack.")
    input()
    print("You hear a voice. The same voice from the beginning of the run.")
    input()
    print("“Welcome, welcome! My name is PSugar400, and congratulations! You’ve defeated the first floor of the dungeon, and we’re all excited for you! Wait, there’s only three of you?” said PSugar400.")
    input()
    print("“Uh, yeah. Tear 2bad was killed by enemies.” you say.")
    input()
    print("“Uh, yeah. MurdleMuffin was killed by enemies.” you say.")
    input()
    print("“Uh, yeah. Eevee005 was killed by enemies.” you say.")
    input()
    print("“Oh, I see. Let us take a moment to remember them.” said PSugar400. “I have a memorial letter written for them. I’ll read it.”")
    input()
    print("“Tear 2bad was one of the first to sign up for our multiplayer test. He was very excited to play in this game. He said he liked the fighting aspect of the test. He wanted to fight Mr. Dye as soon as possible. Let us take a moment of silence.” The crowd was silent for a minute.")
    input()
    print("“We don’t know much about MurdleMuffin’s intentions. They were quite shy, and wouldn’t reveal much. We believe it was to meet new people. Let us take a moment of silence.” The crowd was silent for a minute.")
    input()
    print("“Eevee005 was a sweet person. She was legitimately interested in the project. She was keeping up with updates as soon as we posted an announcement that we were looking for people for a multiplayer test. Let us take a moment of silence.” The crowd was silent for a minute.")
    input()
    input()
    print("“Now, as the audience knows, we have a special guest! The Chosen Hero, %s, is here today with us!” the audience roars with cheers, and %s stands up, waving, and thanking the crowd." % heroName)
    input()
    print("%s looks at you and your team. You notice they look at you suspiciously. But soon looks away." % heroName)
    input()
    print("PSugar400 continues. “But back to our team of playtesters, I think they deserve a name, what do you reckon?” The stands agrees loudly. “How about the Fighting Team?” the audience cheers out the name.")
    input()
    print("“Fighting Team! Fighting Team! Fighting Team! Fighting Team! Fighting Team! Fighting Team!”")
    input()
    print("The Arm Attack Inc. Staff calm down the audience before PSugar400 then keeps talking. ”Now, let’s hear a word from each member from our newly named Fighting Team. %s, you’re up first." & playerName)
    input()
    message = input("Type a message to tell the audience. ")
    print("“%s” you say." % message)
    input()
    print("“Thank you for your words, %s.”" % message)
    input()
    print("“Dino-Pack, what do you have to say?” said PSugar400, continuing her interview.")
    input()
    print("“I ended up finding some really good gear. Gave some extras to other team members. Fighting enemies was really fun! I’d love to do the other floors.” he told PSugar400.")
    input()
    print("“Well, in the future, you might actually be able to do the other 2 floors!” she told Dino-Pack.")
    input()
    print("“And that was the Multiplayer Test for our dungeon here at Arm Attack Inc.! Thank you for being here. Once again, I’ve been your host, PSugar400, and goodnight.” finished PSugar400, walking up to you and your team.")
    input()
    print("“Tomorrow you all have meetings with %s. We’ve given you rooms in a nearby hotel. You all will have reps that you’ll work with. For now, follow the security team. They’ll lead you to your hotel rooms.” informed PSugar400." % heroName)
    input()
    print("You follow PSugar400’s lead along with the rest of your team. Eventually, PSugar400 breaks off the path, and you instead follow a security team. They bring you past crowds, so you wave to them.")
    input()
    print("After a good 10 minutes, you arrive at your hotel room. Room 105. It’s not a very large room, but bigger than general hotel rooms. You look at the time, it’s only 3:00 PM, but you feel very tired. The bed looks very soft and comfortable. You decide to lay down, and then close your eyes, falling into a comfortable sleep.")
    input()
    print("The end.")
    input()
    print("Your game ID is %s. If you play the Floor 2 or Floor 3 games for YAAG Murder Mystery Edition, you’ll need it." % epilogueID)
    input()
    print("Thank you very much for playing my game! It was lots of fun to write, develop and all that. I’ll be making more in the future, so keep an eye out for that!")
    input()
  elif epilogueID == 12:
    print("You regain your vision and see a crowd of people cheering for you and the rest of your team.")
    input()
    print("Then, the crowd starts gasping and murmuring.")
    input()
    print("“They noticed Tear 2bad’s not here.” said MurdleMuffin.")
    input()
    print("“They noticed Dino-Pack’s not here.” said MurdleMuffin.")
    input()
    print("“They noticed Eevee005’s not here.” said MurdleMuffin.")
    input()
    print("You hear a voice. The same voice from the beginning of the run.")
    input()
    print("“Welcome, welcome! My name is PSugar400, and congratulations! You’ve defeated the first floor of the dungeon, and we’re all excited for you! Wait, there’s only three of you?” said PSugar400.")
    input()
    print("“Uh, yeah. Tear 2bad was killed by enemies.” you say.")
    input()
    print("“Uh, yeah. Dino-Pack was killed by enemies.” you say.")
    input()
    print("“Uh, yeah. Eevee005 was killed by enemies.” you say.")
    input()
    print("“Oh, I see. Let us take a moment to remember them.” said PSugar400. “I have a memorial letter written for them. I’ll read it.”")
    input()
    print("“Tear 2bad was one of the first to sign up for our multiplayer test. He was very excited to play in this game. He said he liked the fighting aspect of the test. He wanted to fight Mr. Dye as soon as possible. Let us take a moment of silence.” The crowd was silent for a minute.")
    input()
    print("“Dino-Pack was always interested in Arm Attack Inc.’s projects. He was interested in the original Dungeon run. Trying to talk to %s, trying to talk to us, and all that. The second we saw his request to join the test, we knew he had to be accepted. Let us take a moment of silence.” The crowd was silent for a minute." % heroName)
    input()
    print("“Eevee005 was a sweet person. She was legitimately interested in the project. She was keeping up with updates as soon as we posted an announcement that we were looking for people for a multiplayer test. Let us take a moment of silence.” The crowd was silent for a minute.")
    input()
    input()
    print("“Now, as the audience knows, we have a special guest! The Chosen Hero, %s, is here today with us!” the audience roars with cheers, and %s stands up, waving, and thanking the crowd." % heroName)
    input()
    print("%s looks at you and your team. You notice they look at you suspiciously. But soon looks away." % heroName)
    input()
    print("PSugar400 continues. “But back to our team of playtesters, I think they deserve a name, what do you reckon?” The stands agrees loudly. “How about the Fighting Team?” the audience cheers out the name.")
    input()
    print("“Fighting Team! Fighting Team! Fighting Team! Fighting Team! Fighting Team! Fighting Team!”")
    input()
    print("The Arm Attack Inc. Staff calm down the audience before PSugar400 then keeps talking. ”Now, let’s hear a word from each member from our newly named Fighting Team. %s, you’re up first." % playerName)
    input()
    message = input("Type a message to tell the audience. ")
    print("“%s” you say." % message)
    input()
    print("“Thank you for your words, %s.”" % message)
    input()
    print("“So, MurdleMuffin, what do you think?” asked PSugar400.")
    input()
    print("“It was fine, I kinda just stayed in one room for a while.” he answered simply. “Oh. Well, that’s okay.”")
    input()
    print("“And that was the Multiplayer Test for our dungeon here at Arm Attack Inc.! Thank you for being here. Once again, I’ve been your host, PSugar400, and goodnight.” finished PSugar400, walking up to you and your team.")
    input()
    print("“Tomorrow you all have meetings with %s. We’ve given you rooms in a nearby hotel. You all will have reps that you’ll work with. For now, follow the security team. They’ll lead you to your hotel rooms.” informed PSugar400." % heroName)
    input()
    print("You follow PSugar400’s lead along with the rest of your team. Eventually, PSugar400 breaks off the path, and you instead follow a security team. They bring you past crowds, so you wave to them.")
    input()
    print("After a good 10 minutes, you arrive at your hotel room. Room 105. It’s not a very large room, but bigger than general hotel rooms. You look at the time, it’s only 3:00 PM, but you feel very tired. The bed looks very soft and comfortable. You decide to lay down, and then close your eyes, falling into a comfortable sleep.")
    input()
    print("The end.")
    input()
    print("Your game ID is %s. If you play the Floor 2 or Floor 3 games for YAAG Murder Mystery Edition, you’ll need it." % epilogueID)
    input()
    print("Thank you very much for playing my game! It was lots of fun to write, develop and all that. I’ll be making more in the future, so keep an eye out for that!")
    input()
  elif epilogueID == 13:
    print("You regain your vision and see a crowd of people cheering for you and the rest of your team.")
    input()
    print("Then, the crowd starts gasping and murmuring.")
    input()
    print("“They noticed MurdleMuffin’s not here.” said Tear 2bad.")
    input()
    print("“They noticed Dino-Pack’s not here.” said Tear 2bad.")
    input()
    print("“They noticed Eevee005’s not here.” said Tear 2bad.")
    input()
    print("You hear a voice. The same voice from the beginning of the run.")
    input()
    print("“Welcome, welcome! My name is PSugar400, and congratulations! You’ve defeated the first floor of the dungeon, and we’re all excited for you! Wait, there’s only three of you?” said PSugar400.")
    input()
    print("“Uh, yeah. MurdleMuffin was killed by enemies.” you say.")
    input()
    print("“Uh, yeah. Dino-Pack was killed by enemies.” you say.")
    input()
    print("“Uh, yeah. Eevee005 was killed by enemies.” you say.")
    input()
    print("“Oh, I see. Let us take a moment to remember them.” said PSugar400. “I have a memorial letter written for them. I’ll read it.”")
    input()
    print("“We don’t know much about MurdleMuffin’s intentions. They were quite shy, and wouldn’t reveal much. We believe it was to meet new people. Let us take a moment of silence.” The crowd was silent for a minute.")
    input()
    print("“Dino-Pack was always interested in Arm Attack Inc.’s projects. He was interested in the original Dungeon run. Trying to talk to %s, trying to talk to us, and all that. The second we saw his request to join the test, we knew he had to be accepted. Let us take a moment of silence.” The crowd was silent for a minute." % heroName)
    input()
    print("“Eevee005 was a sweet person. She was legitimately interested in the project. She was keeping up with updates as soon as we posted an announcement that we were looking for people for a multiplayer test. Let us take a moment of silence.” The crowd was silent for a minute.")
    input()
    print("“Now, as the audience knows, we have a special guest! The Chosen Hero, %s, is here today with us!” the audience roars with cheers, and %s stands up, waving, and thanking the crowd." % heroName)
    input()
    print("%s looks at you and your team. You notice they look at you suspiciously. But soon looks away." % heroName)
    input()
    print("PSugar400 continues. “But back to our team of playtesters, I think they deserve a name, what do you reckon?” The stands agrees loudly. “How about the Fighting Team?” the audience cheers out the name.")
    input()
    print("“Fighting Team! Fighting Team! Fighting Team! Fighting Team! Fighting Team! Fighting Team!”")
    input()
    print("The Arm Attack Inc. Staff calm down the audience before PSugar400 then keeps talking. ”Now, let’s hear a word from each member from our newly named Fighting Team. %s, you’re up first." % playerName)
    input()
    message = input("Type a message to tell the audience. ")
    print("“%s” you say." % message)
    input()
    print("“Thank you for your words, %s.”" % playerName)
    input()
    print("Tear 2bad, what would you like to say to the audience?” asked PSugar400.")
    input()
    print("“It. Was. AWESOME! I was slashing enemies, taking them down, I carried the team. Okay, well, maybe not. But still, it was great.” he answered. “Wow, what a story!”")
    input()
    print("“And that was the Multiplayer Test for our dungeon here at Arm Attack Inc.! Thank you for being here. Once again, I’ve been your host, PSugar400, and goodnight.” finished PSugar400, walking up to you and your team.")
    input()
    print("“Tomorrow you all have meetings with %s. We’ve given you rooms in a nearby hotel. You all will have reps that you’ll work with. For now, follow the security team. They’ll lead you to your hotel rooms.” informed PSugar400." % heroName)
    input()
    print("You follow PSugar400’s lead along with the rest of your team. Eventually, PSugar400 breaks off the path, and you instead follow a security team. They bring you past crowds, so you wave to them.")
    input()
    print("After a good 10 minutes, you arrive at your hotel room. Room 105. It’s not a very large room, but bigger than general hotel rooms. You look at the time, it’s only 3:00 PM, but you feel very tired. The bed looks very soft and comfortable. You decide to lay down, and then close your eyes, falling into a comfortable sleep.")
    input()
    print("The end.")
    input()
    print("Your game ID is %s. If you play the Floor 2 or Floor 3 games for YAAG Murder Mystery Edition, you’ll need it." % epilogueID)
    input()
    print("Thank you very much for playing my game! It was lots of fun to write, develop and all that. I’ll be making more in the future, so keep an eye out for that!")
    input()
  elif epilogueID == 14:
    print("You regain your vision, and see a crowd of people cheering for you.")
    input()
    print("Then, the crowd starts gasping and murmuring. ")
    input()
    print("They noticed you’re the only one left. You hear a voice. The same voice from the beginning of the run.")
    input()
    print("“Welcome, welcome! My name is PSugar400, and congratulations! You’ve defeated the first floor of the dungeon, and we’re all excited for you! … Wait, you’re the only one?” said PSugar400.")
    input()
    print("“Uh, yeah. The rest of them were killed by enemies.” you lie, without hesitation.")
    input()
    print("“Oh, I see. Let us take a moment to remember them.” said PSugar400. “I have memorial letters written for them. I’ll read them.”")
    input()
    print("“Tear 2bad was one of the first to sign up for our multiplayer test. He was very excited to play in this game. He said he liked the fighting aspect of the test. He wanted to fight Mr. Dye as soon as possible. Let us take a moment of silence.” The crowd was silent for a minute.")
    input()
    print("“We don’t know much about MurdleMuffin’s intentions. They were quite shy, and wouldn’t reveal much. We believe it was to meet new people. Let us take a moment of silence.” The crowd was silent for a minute")
    input()
    print("“Dino-Pack was always interested in Arm Attack Inc.’s projects. He was interested in the original Dungeon run. Trying to talk to %s, trying to talk to us, and all that. The second we saw his request to join the test, we knew he had to be accepted. Let us take a moment of silence.” The crowd was silent for a minute." % heroName)
    input()
    print("“Eevee005 was a sweet person. She was legitimately interested in the project. She was keeping up with updates as soon as we posted an announcement that we were looking for people for a multiplayer test. Let us take a moment of silence.” The crowd was silent for a minute.")
    input()
    print("“Now, as the audience knows, we have a special guest! The Chosen Hero, %s, is here today with us!” the audience roars with cheers, and they stand up, waving, and thanking the crowd." % heroName)
    input()
    print("%s looks at you. You notice how they look at you suspiciously. It feels like a long time, but ends just as fast as it starts." % heroName)
    input()
    print("PSugar400 continues. “But back to our playtester, I think they deserve a name, what do you reckon?” The stands agrees loudly. “How about the One Person Fighting Team?” Most of the audience stays silent, but some shout your new title.")
    input()
    print("“One Person Fighting Team! One Person Fighting Team! One Person Fighting Team!”")
    input()
    print("Once the crowd calms down, PSugar400 keeps talking. ”Now, let’s hear a word from our newly named One Person Fighting Team. %s, what do you have to say?”" % playerName)
    input()
    message = input("Type a message to tell the audience. ")
    print("“%s” you say." % message)
    input()
    print("“Thank you for your words, %s.”" % playerName)
    input()
    print("“And that was the Multiplayer Test, turned Singleplayer, for our dungeon here at Arm Attack Inc.! Thank you for being here. Once again, I’ve been your host, PSugar400, and goodnight.” finished PSugar400, walking up to you and your team.")
    input()
    print("“Tomorrow you have a meeting with %s. We’ve given you rooms in a nearby hotel. You all will have reps that you’ll work with. For now, follow the security team. They’ll lead you to your hotel rooms.” informed PSugar400." % heroName)
    input()
    print("You follow PSugar400’s lead along. Eventually, PSugar400 breaks off the path, and you instead follow a security team. They bring you past crowds, so you wave to them. Most of the crowd doesn’t wave back.")
    input()
    print("After a good 10 minutes, you arrive at your hotel room. Room 105. It’s not a very large room, but bigger than general hotel rooms. You look at the time, it’s only 3:00 PM, but you feel very tired. The bed looks very soft and comfortable. You decide to lay down, and then close your eyes, falling into a comfortable sleep.")
    input()
    print("The end.")
    input()

  print("This is the end of the current version of the game. Check back soon for a possible update.")
  input()
  print("For now, you win!")
  input()
  print("Thank you for playing. Please report all opinions and/or bugs to me (Dino-Pack).")
  input()
  input("Press enter to close the game. ")
  exit()

if gameOver == "true":
  print("You died!")
  print("")
  input("Press enter to close the game.")
  exit()
