class Printer:
    """
    A basic printer
    
    .. Attention:: There should only be one instance of this class at all times.
    """
    def print(self, message, chat=False, user="", advance=False):
        """
        Print a message to stout. If ``chat`` is ``True``, then print the
        message, video game chat style.

        This function is a wrapper around the ``print()`` function, with a
        ``chat`` flag that indicates whether to print out ``message`` in
        video game chat style. This also has an ``advance`` flag that
        indicates whether to have an ``input()`` function after printing
        out the message.

        Args:
            message (str):
                The message to print.

            chat (bool, optional):
                Whether to print out the message video game chat style,
                defaults to False.

            user (str, optional):
                Only required if ``chat`` is ``True``, the user sending the
                message, defaults to "".

            advance (bool, optional):
                Whether to add an ``input()`` after the ``print()``, thus
                advancing the game, defaults to False.
        """
        if chat:
            print(f"{user}: {message}")
        else:
            print(message)

        if advance:
            input()
