import json

from src.armor import ChainArmor
from src.enums import GameScreen, GameRoom
from src.errors import PlayerNotInitializedError
from src.player import getPlayer
from src.printer import Printer
from src.utils import isBlank
from src.weapons import StoneSword
from src.yaag_parser import parseFile


class Game:
    """
    The main game.
    
    .. Attention:: There should only be one instance of this class at all times.
    """

    def __init__(self):
        self.state = {
            "screen": GameScreen.TITLE,
            "playing": True,
            "room": GameRoom.START,
            "heroName": ""
        }
        self.player = None
        self.printer = Printer()

    def play(self):
        """Start the game!"""
        while self.state["playing"]:
            if self.state["screen"] == GameScreen.TITLE:
                self.titleScreen()
            elif self.state["screen"] == GameScreen.GAME:
                self.game()
            elif self.state["screen"] == GameScreen.EPILOGUE:
                self.epilogue()

    def titleScreen(self):
        """The game's title screen, which decides your player name and introduces the advance mechanic."""
        print("To play this game, you must type your answers. Also, to advance the story, press enter.")
        print("")
        input("Press enter to begin.")

        with open("config.json") as f:
            self.player = getPlayer(self, json.load(f)["codes"])

        # Starting items
        self.player.inventory.add(StoneSword())
        self.player.inventory.add(ChainArmor())

    def game(self):
        """The game's main gameplay."""
        if self.state["room"] == GameRoom.START:
            self.executeYaagFile("states/start/start.yaag")
        self.state["playing"] = False

    def epilogue(self):
        """The game's epilogue."""
        pass

    def executeYaagFile(self, filename):
        if self.player is None:
            raise PlayerNotInitializedError("To parse and execute a YAAG file, "
                                            "the player must be initialized.")
        parsed = parseFile(filename, self.player)
        print("\n\n\n")
        for item in parsed:
            if item["type"] == "dialogue":
                for message in item["messages"]:
                    self.printer.print(message)
            elif item["type"] == "dialogue_advance":
                for message in item["messages"]:
                    self.printer.print(message, advance=True)
            elif item["type"] == "decision":
                print(item["question"])

                for index, option in enumerate(item["options"]):
                    print(f"{index + 1} - {option}")

                print()
                choice = None
                while choice is None:
                    choice = input("Decision: ")
                    if isBlank(choice):
                        print("Please give a valid number!")
                        choice = None
                    elif not choice.isnumeric():
                        print("Please choose a number!")
                        choice = None
                    elif not 0 < int(choice) <= len(item["options"]):
                        print("Please choose an option between 1 and "
                              f"{len(item['options'])} (inclusive)!")
                        choice = None
                    else:
                        choice = int(choice)
                self.executeYaagFile(item["files"][choice])
