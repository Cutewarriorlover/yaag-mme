class Enemy:
    def __init__(self):
        self.name = "Enemy"
        self.stats = {
            "damage": 0,
            "health": 0,
            "healing": 0
        }


class Slime(Enemy):
    def __init__(self):
        super().__init__()
        self.name = "Slime"
