import random
import re
from itertools import takewhile
from typing import List


def changePlaceholders(string: str, player) -> str:
    return string.replace("{{player_name}}", player.name)


def parseFile(path: str, player) -> List[dict]:
    with open(path, 'r') as file:
        return parse(file.read(), player)


def parse(source: str, player) -> List[dict]:
    original_source = source.split("\n")
    source = [i for i in original_source if i != ""]
    parsed = []
    line = 0

    if not source[0].startswith("[") or not source[0].endswith("]"):
        raise TypeError(
            "YAAG files should start with a '[' and end with a ']'")
    if source[0].startswith("[dialogue]"):
        messages = [changePlaceholders(x.strip(), player) for x in takewhile(
            lambda x: x.startswith("    "), source[1:])]

        parsed.append({"type": "dialogue", "messages": messages})
        line += len(messages) + 1
    while line < len(source):
        if source[line].startswith("[dialogue]"):
            messages = [changePlaceholders(x.strip(), player) for x in takewhile(
                lambda x: x.startswith("    "), source[line + 1:])]

            parsed.append({"type": "dialogue", "messages": messages})
            line += len(messages) + 1
        elif source[line].startswith("[dialogue advance]"):
            messages = [changePlaceholders(x.strip(), player) for x in takewhile(
                lambda x: x.startswith("    "), source[line + 1:])]

            parsed.append({"type": "dialogue_advance", "messages": messages})
            line += len(messages) + 1
        elif re.match(r"\[decision \"([a-zA-Z0-9\s,.?!*()]*)\"]", source[line]):
            match = re.match(r"\[decision \"([a-zA-Z0-9\s,.?!*()]*)\"]", source[line])
            options = [re.sub(r"\[.*]", "", x).strip() for x in takewhile(
                lambda x: x.startswith("    "), source[line + 1:])]
            files = [re.sub(r".*\[", "", x).strip()[:-1] for x in takewhile(
                lambda x: x.startswith("    "), source[line + 1:])]

            parsed.append(
                {"type": "decision", "question": match.group(1), "options": options, "files": files})
            line += len(options) + 1
        elif re.match(r"\[room ([a-zA-Z]+)]", source[line]):
            match = re.match(r"\[room ([a-zA-Z]+)]", source[line])

            parsed.append({"type": "changeRom", "room": match.group(1)})

            line += 1
        elif re.match(r"\[quiz \"([a-zA-Z0-9\s,.?!*()]*)\"]", source[line]):
            match = re.match(
                r"\[quiz \"([a-zA-Z0-9\s,.?!*()]*)\"]", source[line])
            params = [re.sub(r"\[.*]", "", x).strip() for x in takewhile(
                lambda x: x.startswith("    "), source[line + 1:])]
            answer = params[0]
            stats = params[1]

            parsed.append({"type": "quiz", "question": match.group(
                1), "answer": answer, "stats": stats})
            line += 3
        elif re.match(r"\[fight [a-zA-Z0-9,\":.\s]+]", source[line]):
            match = re.match(r"\[fight ({\"[a-zA-Z0-9,\":.\s]+})]", source[line])
            name = match.group(1)

            parsed.append({"type": "fight", "info": name})
            line += 1
        elif re.match(r"\[chance \d+]", source[line]):
            match = re.match(r"\[chance (\d+)]", source[line])
            params = [re.sub(r"\[.*]", "", x).strip() for x in takewhile(
                lambda x: x.startswith("    "), source[line + 1:])]
            true = params[0]
            false = params[1]

            if random.randint(0, 100) < int(match.group(1)):
                parsed.append({"type": "chance", "file": true})
            else:
                parsed.append({"type": "chance", "file": false})

            line += 3
        else:
            raise SyntaxError(f"\033[31;1;4mInvalid syntax on line {line}:\033[0m\n\t> {original_source[line]}\033[0m")

    return parsed
