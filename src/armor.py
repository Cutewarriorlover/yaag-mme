class Armor:
    def __init__(self):
        self.defense = 0
        self.name = "Armor"


class ChainArmor(Armor):
    def __init__(self):
        super().__init__()
        self.defense = 3
        self.name = "Chain Armor"


class RubySuit(Armor):
    def __init__(self):
        super().__init__()
        self.defense = 5
        self.name = "Ruby Suit"
